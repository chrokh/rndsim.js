import { add                } from '../actions/generic'
import { currentTick        } from '../selectors/simulation'
import { didReachPOI        } from '../selectors/projects'
import { totalDiscoveryRate } from '../selectors/project-types'
import { totalSize as totalInterventionSize } from '../selectors/interventions'


export default (store) => {

  throw 'TODO: This should not be based on store data, but rather the collated output produced by the summarizer'

  const last = store.get('projectsThatReachedPOI', data) || {
    x: [],
    y: []
  }

  const tick = store.select(currentTick)
  const totalPOIProjects = store.select(didReachPOI).length
  const discoveryRate    = store.select(totalDiscoveryRate)
  const merSize          = store.select(totalInterventionSize)

  const data = {
    x: [...last.x, tick],
    y: [...last.y, totalPOIProjects],
    name: `MER size ${merSize}, discovery ${discoveryRate}`
  }

  store.set('projectsThatReachedPOI', data)
}
