Agent-Base Simulation of Research & Development.


# Installation

- `npm install [-g] rndsim`



# Usage

## Running simulations using CLI

- `$ rndsim [INPUT-FILE] [OUTPUT-FOLDER]`.
- The required structure of the input file is described in `js/schema.js` (available in the repo).


## Using input editor

- Not yet available if installed via npm.



# For Developers

## Running Tests

- `$ npm test`
