var Simulation = {}
Simulation.schema  = RNDSimClient.schema;
Simulation.example = RNDSimClient.example;



var Editor = {}
Editor.getValue = function() {
  return Editor.editor.getValue()
}
Editor.init = function() {
  var element = document.getElementById('input-editor');
  Editor.editor  = new JSONEditor(element, {
    schema:                Simulation.schema,
    theme:                 'bootstrap3',
    iconlib:               'bootstrap3',
    disable_properties:    true,
    display_required_only: true,
    disable_array_delete_last_row: true,
    disable_array_delete_all_rows: true
  });
  // Setting twice because of bug:
  // https://github.com/jdorn/json-editor/issues/364#issuecomment-260134244
  Editor.editor.setValue(Simulation.example)
  Editor.editor.setValue(Simulation.example)
}



Editor.init()
