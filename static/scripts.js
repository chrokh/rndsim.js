
var Simulation = {}
Simulation.example = RNDSimClient.example;
Simulation.schema  = RNDSimClient.schema;

Simulation.setup = function() {
  var input = Editor.getValue()
  this.simulation = new RNDSimClient.Simulation(input, {})


  this.simulation.on('afterTick', function(store) {
    var tick = store.state.simulations[undefined].tick
    Dash.setTick(tick)
  })

  // TODO: Plots
  this.simulation.on('after', function(store) {
    console.log("next")
    const data = store.get('projectsThatReachedPOI')
    Plotter.plot([data])
  })
}
Simulation.start = function() {
  if (this.simulation == null) {
    Simulation.setup();
  }
  this.simulation.start();
}

Simulation.reset = function() {
  this.simulation = null
}

Simulation.stop = function() {
  if (this.simulation != null) {
    this.simulation.stop();
  }
}

Simulation.step = function() {
  if (this.simulation == null) {
    Simulation.setup();
  }
  this.simulation.step();
}

Simulation.loop = function() {
  if (this.simulation == null) {
    Simulation.setup();
  }
  this.simulation.loop();
}





var Dash = {}
Dash.setTick = function(t) {
  document.getElementById('tick').innerHTML = t
}









State = {}
State.init = function() {
  Editor.init()
  State.idle()
  Plotter.init()
}
State.idle = function() {
  Buttons.clear()
  Buttons.start.on()
  Buttons.step.on()
  Buttons.loop.on()
  Buttons.stop.off()
  Buttons.reset.off()
}
State.start = function() {
  Buttons.clear()
  Buttons.start.off()
  Buttons.step.off()
  Buttons.loop.off()
  Buttons.stop.on()
  Buttons.reset.off()
  Simulation.start()
}
State.stop = function() {
  Buttons.clear()
  State.idle()
  Simulation.stop()
  Buttons.reset.on()
}
State.step = function() {
  Buttons.clear()
  Simulation.step()
  State.idle()
  Buttons.reset.on()
}
State.loop = function() {
  Buttons.clear()
  State.stoppable()
  Simulation.loop()
}
State.reset = function() {
  State.idle()
  Simulation.reset()
}
State.stoppable = function() {
  Buttons.clear()
  Buttons.stop.on()
}






Buttons = {}
Buttons._start = document.getElementById('btn-start')
Buttons._stop  = document.getElementById('btn-stop')
Buttons._step  = document.getElementById('btn-step')
Buttons._loop  = document.getElementById('btn-loop')
Buttons._reset  = document.getElementById('btn-reset')

Buttons._turnOn = function(element, callback) {
  element.addEventListener('click', callback)
  element.removeAttribute('disabled')
}
Buttons._turnOff = function(element, callback) {
  element.removeEventListener('click', callback)
  element.setAttribute('disabled', 'disabled')
}

Buttons.clear = function() {
  Buttons.start.off()
  Buttons.stop.off()
  Buttons.step.off()
  Buttons.loop.off()
  Buttons.reset.off()
}

Buttons.start = {};
Buttons.start.on = function() {
  Buttons._turnOn(Buttons._start, State.start)
}
Buttons.start.off = function() {
  Buttons._turnOff(Buttons._start, State.start)
}

Buttons.stop = {};
Buttons.stop.on = function() {
  Buttons._turnOn(Buttons._stop, State.stop)
}
Buttons.stop.off = function() {
  Buttons._turnOff(Buttons._stop, State.stop)
}

Buttons.loop = {}
Buttons.loop.on = function() {
  Buttons._turnOn(Buttons._loop, State.loop)
}
Buttons.loop.off = function() {
  Buttons._turnOff(Buttons._loop, State.loop)
}

Buttons.step = {}
Buttons.step.on = function() {
  Buttons._turnOn(Buttons._step, State.step)
}
Buttons.step.off = function() {
  Buttons._turnOff(Buttons._step, State.step)
}

Buttons.reset = {}
Buttons.reset.on = function() {
  Buttons._turnOn(Buttons._reset, State.reset)
}
Buttons.reset.off = function() {
  Buttons._turnOff(Buttons._reset, State.reset)
}





var Collector = {}
Collector.add = function(key, state, f) {
  Collector.data = Collector.data || {}
  Collector.data[key] = Collector.data[key] || []
  var existing = Collector.data[key]
  Collector.data[key].push(f(state, existing))
}
Collector.get = function(key) {
}








var Plotter = {}
Plotter.init = function() {
}
Plotter._element = document.getElementById('plot-area')
Plotter.plot = function(data) {
  Plotly.plot(this._element, data)
}
Plotter.test = function() {
  //Plotly.plot(this._element, [
  //  {
  //    x: [1, 2, 3, 4, 5],
  //    y: [1, 2, 4, 8, 16],
  //    name: "barbar"
  //  },
  //  {
  //    x: [1, 2, 3, 4, 5],
  //    y: [10, 20, 30, 40],
  //    name: "foo"
  //  },
  //]);
}





Editor = {}
Editor.getValue = function() {
  return Editor.editor.getValue()
}
Editor.init = function() {
  var element = document.getElementById('input-editor');
  Editor.editor  = new JSONEditor(element, {
    schema:                Simulation.schema,
    theme:                 'bootstrap3',
    iconlib:               'bootstrap3',
    disable_properties:    true,
    display_required_only: true,
    disable_array_delete_last_row: true,
    disable_array_delete_all_rows: true
  });
  // Setting twice because of bug:
  // https://github.com/jdorn/json-editor/issues/364#issuecomment-260134244
  Editor.editor.setValue(Simulation.example)
  Editor.editor.setValue(Simulation.example)
}




function init() {
  State.init()
}


init()
