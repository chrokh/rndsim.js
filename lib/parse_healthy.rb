require_relative './parse.rb'
require 'csv'

data   = CSV.read('./output/RUNS_TICKS_PROJECTS.csv')
result = parse(data, 12)
CSV.open("./output/HEALTHY.csv", "wb") do |csv|
  result.each do |line|
    csv << line
  end
end
