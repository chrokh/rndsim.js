
def as_hash data
  keys = data.first
  arr = data.map.with_index do |row, row_index|
    keys.map.with_index do |key, col_index|
      Hash[key, data[row_index][col_index]]
    end.reduce(&:merge)
  end
  arr.shift
  arr
end



def per_run data
  runs = {}
  data.each do |row|
    runs[row["RUN"]] ||= []
    runs[row["RUN"]] << row
  end
  runs
end


def remove_rows data
  run_column = data.first.index("RUN")

end

def subset_by data, columns, column
  index = columns.index(column)
  result = {}
  data.each do |row|
    result[row[index]] ||= []
    result[row[index]] << row
  end
  result
end



def by_run_by_tick data, columns
  by_run = subset_by(data, columns, 'RUN')
  by_run.map do |key, value|
    Hash[key, subset_by(value, columns, 'TICK')]
  end.reduce(&:merge)
end


def projects_at_market_analysis data, columns
  result = data.map do |key, value|

    value = value.map do |tick_key, tick_value|
      target = "InPatentMarket year1"
      column = columns.index("proj_stage_type")

      condition = tick_value.reduce(false) do |result, tick|
        result || tick[column] == target
      end

      Hash[tick_key, {
        has_project_at_market: condition
      }]
    end.reduce(&:merge)

    Hash[key, value]
  end.reduce(&:merge)
end


def only_with_projects_at_market data, columns
  data.map do |key, value|

    # find first occurance of
    value = value.map do |tick_key, tick_value|
      target = "InPatentMarket year1"
      column = columns.index("proj_stage_type")

      condition = tick_value.reduce(false) do |result, tick|
        result || tick[column] == target
      end

      if condition then tick_key else nil end
    end.compact

    Hash[key, value]
  end.reduce(&:merge)
end


def only_consecutive data, columns, cycle
  hash = data.map do |run_key, run_value|
    start = start_of_consec(only_ticks_with_market_entries(run_value), cycle).to_s
    first = first_to_market(only_ticks_with_market_entries(run_value)).to_s
    Hash[run_key, {
      start: start,
      first: first,
    }]
  end.reduce(&:merge)
end


def only_ticks_with_market_entries run
  run = run.select do |key, value|
    value[:has_project_at_market]
  end.map do |tick_key, tick_info|
    tick_key
  end
end


def first_to_market run
  if run.empty?
    -1
  else
    run.first.to_i
  end
end


def start_of_consec array, cycle
  first = -1

  array.sort.reverse.each_cons(2) do |nxt, prev|
    if nxt.to_i <= prev.to_i + cycle
      first = prev.to_i
    else
      break
    end
  end

  first
end


def extract_value_at_col observation, columns, column_name
  observation[columns.index(column_name)]
end


def tablize runs, data, columns
  require 'pp'

  in_cols  = ['RUN', 'proj_type_discovery_rate', 'interventions_tot_size']
  out_cols = in_cols + ['start_of_consec', 'first_to_market']

  table = []

  # add headers
  table << out_cols

  runs.each do |run_key, run_value|
    observation = data[run_key][data[run_key].keys.first].first

    values = in_cols.map do |col|
      extract_value_at_col(observation, columns, col)
    end

    values << run_value[:start]
    values << run_value[:first]

    table << values
  end

  table

  #runs.each do |run_key, run_value|
  #  observation = data[run_key][data[run_key].keys.first].first
  #  table << (in_cols.map do |col|
  #    observation[columns.index(col)]
  #  end << run[:start].to_s)
  #end

end

def parse data, cycle
  columns = data.first

  # Remove columns from data
  data.shift

  #
  # ACTUAL WORK
  hash    = by_run_by_tick(data, columns)
  data    = projects_at_market_analysis(hash, columns)
  consecs = only_consecutive(data, columns, cycle)
  table   = tablize(consecs, hash, columns)
  table
end
