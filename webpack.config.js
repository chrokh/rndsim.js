var webpack = require('webpack');
var debug   = process.env.NODE_ENV !== "production";

module.exports = [
  {
    name: 'rnd sim, output to ./build',
    context: __dirname,
    devtool: debug ? "eval" : null,

    // Needed to allow loading fs
    // https://github.com/josephsavona/valuable/issues/9
    // http://jlongster.com/Backend-Apps-with-Webpack--Part-I
    target: 'node',

    entry: ["babel-polyfill", "./js/cli.js"],
    output: {
      path: __dirname + "/build",
      filename:      "cli.min.js"
    },
    module: {
      loaders: [
        {
          test: /\.js$/,
          exclude: /(node_modules|bower_components)/,
          loader: 'babel-loader',
          query: {
            presets: ['env'],
            retainLines: true
          }
        }
      ]
    },
    plugins: debug ? [] : [
      new webpack.optimize.DedupePlugin(),
      new webpack.optimize.OccurenceOrderPlugin(),
      new webpack.optimize.UglifyJsPlugin({ mangle: false, sourcemap: false }),
    ],
  },

  {
    name: 'client side, output to ./build',
    entry: './js/client.js',
    output: {
      libraryTarget: 'var',
      library:       'RNDSimClient',
      filename:      './build/client.js'
    }
  }
];
