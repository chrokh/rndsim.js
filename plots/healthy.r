data = read.csv('./output/HEALTHY.csv')
head(data)



png('./output/plots/discovery-vs-interventions-1.png', width=1200, height=1000, res=170)
d = subset(data, start_of_consec != -1)
plot(d$proj_type_discovery_rate, d$interventions_tot_size,
     main = "Conditions for healthiness (1)",
     xlab = 'Discovery rate',
     ylab = 'Intervention size')

png('./output/plots/discovery-vs-interventions-2.png', width=1200, height=1000, res=170)
d = subset(data, start_of_consec != -1)
plot(d$interventions_tot_size, d$proj_type_discovery_rate,
     main = "Conditions for healthiness (2)",
     xlab = 'Intervention size',
     ylab = 'Discovery rate')




png('./output/plots/discovery-vs-start-1.png', width=1200, height=1000, res=170)
d = subset(data, start_of_consec != -1)
plot(d$proj_type_discovery_rate, d$start_of_consec / 12,
     main = "Effect of discovery on first consecutive (1)",
     xlab = 'Discovery rate',
     ylab = 'Start of consecutive period')

png('./output/plots/discovery-vs-start-2.png', width=1200, height=1000, res=170)
d = subset(data, start_of_consec != -1)
plot(d$start_of_consec / 12, d$proj_type_discovery_rate,
     main = "Effect of discovery on first consecutive (1)",
     xlab = 'Start of consecutive period',
     ylab = 'Discovery rate')



png('./output/plots/intervention-vs-start-1.png', width=1200, height=1000, res=170)
d = subset(data, start_of_consec != -1)
plot(d$interventions_tot_size, d$start_of_consec / 12,
     main = "Effect of intervention size on first consecutive (1)",
     xlab = 'Intervention size',
     ylab = 'Start of consecutive period')

png('./output/plots/intervention-vs-start-2.png', width=1200, height=1000, res=170)
d = subset(data, start_of_consec != -1)
plot(d$start_of_consec / 12, d$interventions_tot_size,
     main = "Effect of intervention size on first consecutive (2)",
     xlab = 'Start of consecutive period',
     ylab = 'Intervention size')



png('./output/plots/discovery-vs-time-to-market-1-1.png', width=1200, height=1000, res=170)
d = subset(data, first_to_market != -1)
plot(d$proj_type_discovery_rate, d$first_to_market/12,
     main = "Effect of discovery on time of 1st to market (filtered)",
     sub  = "(Zeros removed)",
     xlab = 'Discovery rate',
     ylab = 'First to market (year)')

png('./output/plots/discovery-vs-time-to-market-1-2.png', width=1200, height=1000, res=170)
d = data
plot(d$proj_type_discovery_rate, d$first_to_market/12,
     main = "Effect of discovery on time of 1st to market (not filtered)",
     xlab = 'Discovery rate',
     ylab = 'First to market (year)')

png('./output/plots/discovery-vs-time-to-market-2-1.png', width=1200, height=1000, res=170)
d = subset(data, first_to_market != -1)
plot(d$first_to_market/12, d$proj_type_discovery_rate,
     main = "Effect of discovery on time of 1st to market (filtered)",
     sub  = "(Zeros removed)",
     xlab = 'First to market (year)',
     ylab = 'Discovery rate')

png('./output/plots/discovery-vs-time-to-market-2-2.png', width=1200, height=1000, res=170)
d = data
plot(d$first_to_market/12, d$proj_type_discovery_rate,
     main = "Effect of discovery on time of 1st to market (not filtered)",
     sub  = "(Zeros not removed)",
     xlab = 'First to market (year)',
     ylab = 'Discovery rate')




png('./output/plots/interventions-vs-time-to-market-1-1.png', width=1200, height=1000, res=170)
d = subset(data, first_to_market != -1)
plot(d$interventions_tot_size, d$first_to_market/12,
     main = "Effect of intervention size on time of 1st to market (filtered)",
     xlab = 'Intervention size',
     ylab = 'First to market (year)')

png('./output/plots/interventions-vs-time-to-market-1-2.png', width=1200, height=1000, res=170)
plot(data$interventions_tot_size, data$first_to_market/12,
     main = "Effect of intervention size on time of 1st to market (not filtered)",
     xlab = 'Intervention size',
     ylab = 'First to market (year)')

png('./output/plots/interventions-vs-time-to-market-2-1.png', width=1200, height=1000, res=170)
d = subset(data, first_to_market != -1)
plot(d$first_to_market/12, d$interventions_tot_size,
     main = "Effect of intervention size on time of 1st to market (filtered)",
     xlab = 'First to market (year)',
     ylab = 'Intervention size')

png('./output/plots/interventions-vs-time-to-market-2-2.png', width=1200, height=1000, res=170)
d = data
plot(d$first_to_market/12, d$interventions_tot_size,
     main = "Effect of intervention size on time of 1st to market (not filtered)",
     xlab = 'First to market (year)',
     ylab = 'Intervention size')
