############## DEPENDENCIES #############
library(data.table)
library(plyr)
library(RColorBrewer)
#########################################


############## READ DATA #############
cols <- c('RUN', 'TICK', 'proj_is_at_poi', 'proj_group', 'interventions_tot_size')
EX05 <- fread("/Volumes/LaCie\ HFS+/Simulations/EX_5.csv", sep = ",", header = TRUE, stringsAsFactors = FALSE, select = cols)
EX06 <- fread("/Volumes/LaCie\ HFS+/Simulations/EX_6.csv", sep = ",", header = TRUE, stringsAsFactors = FALSE, select = cols)
EX07 <- fread("/Volumes/LaCie\ HFS+/Simulations/EX_7.csv", sep = ",", header = TRUE, stringsAsFactors = FALSE, select = cols)
EX08 <- fread("/Volumes/LaCie\ HFS+/Simulations/EX_8.csv", sep = ",", header = TRUE, stringsAsFactors = FALSE, select = cols)
EX09 <- fread("/Volumes/LaCie\ HFS+/Simulations/EX_9.csv", sep = ",", header = TRUE, stringsAsFactors = FALSE, select = cols)
EX10 <- fread("/Volumes/LaCie\ HFS+/Simulations/EX_10.csv", sep = ",", header = TRUE, stringsAsFactors = FALSE, select = cols)
###################################


# Prepare for plotting
p4.df05 <- prepare(EX05)
p4.df06 <- prepare(EX06)
p4.df07 <- prepare(EX07)
p4.df08 <- prepare(EX08)
p4.df09 <- prepare(EX09)
p4.df10 <- prepare(EX10)

# Print many at the same time?
par(mfrow=c(3, 1))

# Plot 1
ylim <- c(0, 30)
p4.plot1(p4.df05, datasource = '(EX 05)', ylim = ylim)
p4.plot1(p4.df06, datasource = '(EX 06)', ylim = ylim)
p4.plot1(p4.df07, datasource = '(EX 07)', ylim = ylim)
p4.plot1(p4.df08, datasource = '(EX 08)', ylim = ylim)
p4.plot1(p4.df09, datasource = '(EX 09)', ylim = ylim)
p4.plot1(p4.df10, datasource = '(EX 10)', ylim = ylim)




########### PREPARATION ##########
p4.count_entries <- function(proj_is_at_poi) {
  sum(ifelse(proj_is_at_poi == 'true', 1, 0))
}
p4.colorscale <- function(df) {
  rev(brewer.pal(nlevels(factor(df$proj_group)), "Set1"))
}
p4.prepare <- function(df) {
  ddply(df, c('RUN', 'proj_group', 'interventions_tot_size'), summarise, num_pois = p4.count_entries(proj_is_at_poi))
}
###############################



########### PLOTTING ##########
p4.plot1 <- function(df, datasource = '', ylim = NULL) {
  df$color <- p4.colorscale(df)[factor(df$proj_group)]
  plot(df$num_pois ~ df$interventions_tot_size,
       col     = df$color,
       main    = paste('Total market entries per group and intervention size', datasource),
       ylab    = 'Number of entries',
       xlab    = 'Intervention size',
       #las     = 2,
       pch     = 17,
       ylim    = ylim
  )
  grid(nx = 0, ny = NULL, col = 'darkgray', lty = 'dotted', equilogs = TRUE)
  legend('topleft', levels(factor(df$proj_group)), pch = 15, col = df$color, bty = 'n', title="Type")
}
###############################






