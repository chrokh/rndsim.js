############## DEPENDENCIES #############
library(data.table)
library(plyr)
library(RColorBrewer)
#########################################


########### FUNCTIONS ################
plot1 <- function(df, ylim = NULL, heading = '') {
  df$color   <- colorscale(df)[factor(df$experiment)]
  categories <- length(unique(df$experiment))
  boxcolors  <- rep(head(colorscale(df), categories), categories)
  plot(df$num_pois ~ interaction(df$experiment, df$intervention_bin_mid),
       boxfill = boxcolors,
       main    = paste('Market entries per experiment', heading),
       xlab    = 'Intervention size (+/- 250)',
       ylab    = 'Market entries of type',
       ylim    = ylim,
       las     = 2,
       xaxt    = 'n'
  )
  grid(nx = 0, ny = NULL, col = 'darkgray', lty = 'dotted', equilogs = TRUE)
  pos_after_group <- seq(from = nlevels(factor(df$experiment)) + 0.5,
                         to = (nlevels(factor(df$experiment)) * length(unique(df$intervention_bin))),
                         by = nlevels(factor(df$experiment)))
  abline(h = NULL, v = pos_after_group, col = 'black', lty = 'solid')
  total <- length(unique(df$intervention_bin)) * length(unique(df$experiment))
  axis(1, at=seq(from = length(unique(df$experiment)) / 2,
                 to = total,
                 by = length(unique(df$experiment))),
       labels = sort(unique(df$intervention_bin_mid)))
  legend('topleft', levels(factor(df$experiment)), pch = 15, col = boxcolors, bty = 'n',
         y.intersp=0.4)
}

prepare <- function(intervention_bins) {
  # DROP EVERYTHING OUTSIDE OF BIN EDGES
  df <- subset(df, df$interventions_tot_size >= min(intervention_bins) & df$interventions_tot_size <= max(intervention_bins))
  # ADD BIN INFO
  df$intervention_bin      <- findInterval(df$interventions_tot_size, intervention_bins)
  df$intervention_bin_top  <- intervention_bins[df$intervention_bin + 1]
  df$intervention_bin_bot  <- intervention_bins[df$intervention_bin]
  df$intervention_bin_mid  <- df$intervention_bin_bot + (df$intervention_bin_top - df$intervention_bin_bot) / 2
}

prepare_single <- function(df, groups) {
  df <- subset(df, df$proj_group %in% groups)
  df <- ddply(df, c('RUN', 'intervention_bin', 'intervention_bin_top', 'intervention_bin_bot', 'intervention_bin_mid', 'experiment'), summarise,
              num_pois = count_entries(proj_is_at_poi))
  return(df)
}

add_experiment_name <- function(df, name) {
  df$experiment <- name
  return(df)
}

count_entries <- function(proj_is_at_poi) {
  sum(ifelse(proj_is_at_poi == 'true', 1, 0))
}

colorscale <- function(df) {
  rev(brewer.pal(nlevels(factor(df$experiment)), "Set1"))
}
########################################






############## READ DATA #############
cols <- c('RUN', 'TICK', 'proj_is_at_poi', 'proj_group', 'interventions_tot_size')
EX05 <- fread("/Volumes/LaCie\ HFS+/Simulations/EX_5.csv", sep = ",", header = TRUE, stringsAsFactors = FALSE, select = cols)
EX06 <- fread("/Volumes/LaCie\ HFS+/Simulations/EX_6.csv", sep = ",", header = TRUE, stringsAsFactors = FALSE, select = cols)
EX07 <- fread("/Volumes/LaCie\ HFS+/Simulations/EX_7.csv", sep = ",", header = TRUE, stringsAsFactors = FALSE, select = cols)
EX08 <- fread("/Volumes/LaCie\ HFS+/Simulations/EX_8.csv", sep = ",", header = TRUE, stringsAsFactors = FALSE, select = cols)
EX09 <- fread("/Volumes/LaCie\ HFS+/Simulations/EX_9.csv", sep = ",", header = TRUE, stringsAsFactors = FALSE, select = cols)
EX10 <- fread("/Volumes/LaCie\ HFS+/Simulations/EX_10.csv", sep = ",", header = TRUE, stringsAsFactors = FALSE, select = cols)
###################################

# PREPARE
df.collected <- rbind(
  add_experiment_name(EX05, 'Ex05'),
  add_experiment_name(EX06, 'Ex06'),
  add_experiment_name(EX07, 'Ex07'),
  add_experiment_name(EX08, 'Ex08'),
  add_experiment_name(EX09, 'Ex09'),
  add_experiment_name(EX10, 'Ex10')
)
df <- prepare(df.collected, seq(from = 0, to = 2000, by = 500))

df.abc <- prepare_single(df, c('A', 'B', 'C'))
df.ab  <- prepare_single(df, c('A', 'B'))
df.bc  <- prepare_single(df, c('B', 'C'))
df.a   <- prepare_single(df, c('A'))
df.b   <- prepare_single(df, c('B'))
df.c   <- prepare_single(df, c('C'))

# PLOT
par(mfrow=c(3, 1))
plot1(df.a, ylim = c(0, 50), heading = '(type A)')
plot1(df.b, ylim = c(0, 50), heading = '(type B)')
plot1(df.c, ylim = c(0, 50), heading = '(type C)')
plot1(df.abc, ylim = c(0, 50), heading = '(all types)')
plot1(df.ab, ylim = c(0, 50), heading = '(type A + B)')
plot1(df.bc, ylim = c(0, 50), heading = '(type B + C)')
################################



