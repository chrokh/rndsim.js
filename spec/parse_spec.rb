require_relative '../lib/parse.rb'

describe 'parsing' do

  let(:data) {
    [
      [
        "RUN",
        "TICK",
        "PROJ",
        "interventions_tot_size",
        "proj_type_discovery_rate",
        "proj_stage_type",
      ],
      [ "0",  "00",  "1",  "000",  "00", "***" ],
      [ "0",  "01",  "1",  "000",  "00", "InPatentMarket year1" ],
      [ "0",  "02",  "1",  "000",  "00", "InPatentMarket year1" ],
      [ "0",  "03",  "1",  "000",  "00", "***" ],
      [ "0",  "04",  "1",  "000",  "00", "InPatentMarket year1" ],
      [ "0",  "05",  "1",  "000",  "00", "InPatentMarket year1" ],
      [ "1",  "10",  "1",  "111",  "11", "***" ],
      [ "1",  "11",  "1",  "111",  "11", "InPatentMarket year1" ],
      [ "1",  "12",  "1",  "111",  "11", "InPatentMarket year1" ],
      [ "1",  "13",  "1",  "111",  "11", "InPatentMarket year1" ],
      [ "2",  "20",  "1",  "222",  "22", "***" ],
      [ "2",  "21",  "1",  "222",  "22", "***" ],
      [ "3",  "30",  "1",  "333",  "33", "***" ],
      [ "3",  "31",  "1",  "333",  "33", "InPatentMarket year1" ],
      [ "3",  "32",  "1",  "333",  "33", "InPatentMarket year1" ],
      [ "3",  "33",  "1",  "333",  "33", "InPatentMarket year1" ],
    ]
  }

  let(:expected) {
    [
      ["RUN", "proj_type_discovery_rate", "interventions_tot_size", "start_of_consec", "first_to_market"],
      ["0", "00", "000", "4", "1"],
      ["1", "11", "111", "11", "11"],
      ["2", "22", "222", "UNDEFINED", "UNDEFINED"],
      ["3", "33", "333", "31", "31"],
    ]
  }

  it 'parses' do
    result = parse(data, 1)
    expect(result[0]).to eq(expected[0])
    expect(result[1]).to eq(expected[1])
    expect(result[2]).to eq(expected[2])
    expect(result[3]).to eq(expected[3])
    expect(result).to eq(expected)
  end

  context '#start_of_consec' do
    let(:cycle) { 2 }
    subject { start_of_consec(array, cycle) }

    context 'when consec' do
      let(:array) { [0,2,4,10,12,14,16] }
      it { is_expected.to eq(10) }
    end

    context 'when not consec' do
      let(:array) { [0,2,4,10,12,14,16,19] }
      it { is_expected.to eq('UNDEFINED') }
    end
  end

end
