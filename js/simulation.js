const _ = require('lodash')

const validate      = require('./synthesizers/validate.js')
const normalize     = require('./synthesizers/normalizer.js').normalize

const Randomizer    = require('./endpoints/randomizer.js')
const Store         = require('./store')

const lastTick      = require('./selectors/simulation').lastTick
const lastRun       = require('./selectors/simulation').lastRun
const currentTick   = require('./selectors/simulation').currentTick
const currentRun    = require('./selectors/simulation').currentRun

const incrementTick = require('./actions/simulation').incrementTick
const incrementRun  = require('./actions/simulation').incrementRun

const recalculateCostOfCapital = require('./rules/recalculate-cost-of-capital')
const correctForInflation  = require('./rules/correct-for-inflation')
const sample               = require('./rules/sample')
const spawnProjects        = require('./rules/spawn-projects')
const spawnGrants          = require('./rules/spawn-grants')
const spawnInvestors       = require('./rules/spawn-investors')
const spawnInterventions   = require('./rules/spawn-interventions')
const spawnOrganizations   = require('./rules/spawn-organizations')
const purgeOrganizations   = require('./rules/purge-organizations')
const discovery            = require('./rules/discovery')
const offspinning          = require('./rules/offspinning')
const grants               = require('./rules/grants')
const stateChanges         = require('./rules/state-changes')
const initialization       = require('./rules/initialization')



module.exports = class Simulation {

  constructor(input, config) {

    // Set parameters
    this.input  = input
    this.config = Object.assign({
      sleep: 0 // Optional sleep between ticks
    }, config)

    // Defaults
    this.input.tick = this.input.tick || 0
    this.input.run  = this.input.run || 1
    this.input.first_run  = this.input.run
    this.input.first_tick = this.input.tick

    // Setup callback containers
    this.callbacks = {
      before:      [],
      after:       [],
      beforeTick:  [],
      tick:        [],
      afterTick:   []
    }

    // Create randomizer
    this.randomizer = new Randomizer(input.seed)

    // Add initiation rules
    this.on('before', correctForInflation)
    this.on('before', recalculateCostOfCapital)
    this.on('before', sample)
    this.on('before', spawnInvestors)
    this.on('before', spawnGrants)
    this.on('before', spawnOrganizations)
    this.on('before', spawnInterventions)
    this.on('before', spawnProjects)

    // Add rules
    this.addRule(purgeOrganizations)
    this.addRule(grants)
    this.addRule(stateChanges)
    this.addRule(discovery)
    this.addRule(offspinning)
    this.addRule(initialization) // Set project states for next tick
  }



  /*
   * Register event listeners
   */
  on(name, callback) {
    this.callbacks[name].push(callback);
  }


  /*
   * Register rules
   */
  addRule(f) {
    this.on('tick', f);
  }


  /*
   * Simulation control
   */

  reset() {
    if (this.store != null && this.store != undefined) {
      // TODO: This is a hack to carry run over resets of the store
      this.input.run = this.store.select(currentRun)
    }

    this.store = null
  }

  setup() {
    if (this.store == null) {
      this.store = new Store(
        normalize(
          validate(
            JSON.parse(JSON.stringify(this.input)) // Deep clone input
          )))
      this.callbacks.before.forEach(cb => { cb(this.store, this.randomizer) })
    }
  }

  start() {
    this.stopped = false
    this.setup()
    this.stepAll()
  }

  step() {
    this.setup()

    this.callbacks.beforeTick.forEach(f => f(this.store, this.randomizer))
    this.callbacks.tick.forEach(f => f(this.store, this.randomizer))
    this.store.dispatch(incrementTick)
    this.callbacks.afterTick.forEach(f => f(this.store, this.randomizer))
    this.checkIfDone()
  }

  stop() {
    this.stopped = true
  }

  stepAll(callback = () => {}) {
    this.setup()
    if (!this.isDone() && !this.stopped) {
      this.step()
      setTimeout(() => this.stepAll(callback), this.config.sleep)
    } else if (!this.stopped) {
      callback()
    }
  }

  loop() {
    this.stopped = false
    this.setup()
    this.stepAll(() => {
      if (this.store.select(currentRun) < this.store.select(lastRun)) {
        this.store.dispatch(incrementRun)
        this.reset()
        this.loop()
      }
    })
  }



  isDone () {
    return this.store.select(currentTick) > this.store.select(lastTick)
  }

  checkIfDone() {
    if (this.isDone()) {
      this.callbacks.after.forEach(cb => { cb(this.store, this.randomizer) })
    }
  }

}
