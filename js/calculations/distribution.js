const expectedValue = (dist) => {

  // point
  if (!isNaN(dist)) {
    return dist
  }

  // points
  else if (Array.isArray(dist)) {
    if (isNaN(dist[0])) {
      return dist.reduce((sum, elem) =>
        sum + elem.prob * expectedValue(elem.value), 0)
    } else {
      return dist.reduce((sum, x) => sum + x, 0) / dist.length
    }
  }

  // triangular
  else if (dist.mid != null) {
    return dist.mid
  }

  // uniform
  else if (dist.max != null) {
    return (dist.min + dist.max) / 2
  }

}


// TODO: Distribution knowledge is spread both here and in randomizer
const transform = (dist, f) => {

  // triangular
  if (dist.mid != null) {
    return {
      min: f(dist.min),
      mid: f(dist.mid),
      max: f(dist.max)
    }
  }

  // uniform
  else if (dist.max != null) {
    return {
      min: f(dist.min),
      max: f(dist.max)
    }
  }

  // points
  else if (Array.isArray(dist)) {
    let newDist = []

    for (const elem of dist) {
      if (isNaN(elem)) {
        newDist.push({
          prob:  elem.prob,
          value: transform(elem.value, f)
        })
      } else {
        newDist.push(f(elem))
      }
    }

    return newDist
  }

  else {
    // interpolation
    if (isNaN(dist)) {
      return dist

    // point
    } else {
      return f(dist)
    }
  }

}



module.exports = {
  expectedValue,
  transform,
}
