const expect = require('chai').expect
const Module = require('./distribution')

describe('Calculations.Distribution.expectedValue', () => {

  it('handles array of points', () => {
    const input = [10, 20, 30, 40]
    expect(Module.expectedValue(input)).to.eq(25)
  })

  it('handles array of points', () => {
    const input = [
      { prob: 0.5,  value: 1 },
      { prob: 0.25, value: 10 },
      { prob: 0.25, value: 100 },
    ]
    expect(Module.expectedValue(input)).to.eq(28)
  })

  it('handles array of distributions', () => {
    const input = [
      { prob: 0.5,  value: { min: 10, max: 20 } },
      { prob: 0.25, value: { min: 100, mid: 200, max: 300 } },
      { prob: 0.25, value: 1000 }
    ]
    expect(Module.expectedValue(input)).to.eq(307.5)
  })

  it('handles array of distributions', () => {
    const input = [
      { prob: 0.5,  value: { min: 10, max: 20 } },
      { prob: 0.25, value: { min: 100, mid: 200, max: 300 } },
      { prob: 0.25, value: 0 }
    ]
    expect(Module.expectedValue(input)).to.eq(57.5)
  })

})
