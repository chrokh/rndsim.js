const expect  = require('chai').expect
const Periods = require('./periods')


describe('Periods.reducedByPantentLife', () => {
  const f = Periods.reducedByPatentLife

  context('when all stages require patent', () => {
    const input = [
      { id: 1, requires_patent: true },
      { id: 2, requires_patent: true },
      { id: 3, requires_patent: true },
      { id: 4, requires_patent: true },
      { id: 5, requires_patent: true },
    ]

    it('is reduced by patent life', () => {
      expect(f(input, 100).map(i => i.id)).to.eql([1,2,3,4,5])
      expect(f(input, 5).map(i => i.id)).to.eql([1,2,3,4,5])
      expect(f(input, 4).map(i => i.id)).to.eql([1,2,3,4])
      expect(f(input, 3).map(i => i.id)).to.eql([1,2,3])
      expect(f(input, 2).map(i => i.id)).to.eql([1,2])
      expect(f(input, 1).map(i => i.id)).to.eql([1])
      expect(f(input, 0).map(i => i.id)).to.eql([])
      expect(f(input, -10).map(i => i.id)).to.eql([])
    })
  })

  context('when some stages do not require patent', () => {
    const input = [
      { id: 1, requires_patent: true },
      { id: 2, requires_patent: true },
      { id: 3, requires_patent: true },
      { id: 4, requires_patent: false },
      { id: 5, requires_patent: false },
      { id: 6, requires_patent: false },
      { id: 7, requires_patent: true },
    ]

    it('is reduced by patent life as soon as patent life is required', () => {
      expect(f(input, 20).map(i => i.id)).to.eql([1,2,3,4,5,6,7])
      expect(f(input, 7).map(i => i.id)).to.eql([1,2,3,4,5,6,7])
      expect(f(input, 6).map(i => i.id)).to.eql([1,2,3,4,5,6])
      expect(f(input, 5).map(i => i.id)).to.eql([1,2,3,4,5,6])
      expect(f(input, 4).map(i => i.id)).to.eql([1,2,3,4,5,6])
      expect(f(input, 3).map(i => i.id)).to.eql([1,2,3,4,5,6])
      expect(f(input, 2).map(i => i.id)).to.eql([1,2])
      expect(f(input, 1).map(i => i.id)).to.eql([1])
      expect(f(input, 0).map(i => i.id)).to.eql([])
      expect(f(input, -2).map(i => i.id)).to.eql([])
    })
  })

  context('when patent requiring and non-requiring stages are intertwined', () => {
    const input = [
      { id: 1, requires_patent: false },
      { id: 2, requires_patent: true },
      { id: 3, requires_patent: false },
      { id: 4, requires_patent: true },
      { id: 5, requires_patent: false },
    ]

    it('is reduced by patent life as soon as patent life is required', () => {
      expect(f(input, 5).map(i => i.id)).to.eql([1,2,3,4,5])
      expect(f(input, 4).map(i => i.id)).to.eql([1,2,3,4,5])
      expect(f(input, 3).map(i => i.id)).to.eql([1,2,3])
      expect(f(input, 2).map(i => i.id)).to.eql([1,2,3])
      expect(f(input, 1).map(i => i.id)).to.eql([1])
      expect(f(input, 0).map(i => i.id)).to.eql([1])
      expect(f(input,-1).map(i => i.id)).to.eql([1])
    })
  })

  context('when patent required parameter is missing', () => {
    it('throws', () => {
      const input = [
        { requires_patent: false },
        {                        },
        { requires_patent: false },
      ]
      expect(() => f(input, 0, 5)).to.throw(Error)
    })
  })

  context('when step is undefined', () => {
    it('treats it as if patent life was not required', () => {
      const input = [
        { id: 1, requires_patent: true },
        undefined,
        { id: 3, requires_patent: true },
      ]
      expect(f(input, 3).map(i => i ? i.id : i)).to.eql([1,undefined,3])
      expect(f(input, 2).map(i => i ? i.id : i)).to.eql([1,undefined])
      expect(f(input, 1).map(i => i ? i.id : i)).to.eql([1,undefined])
      expect(f(input, 0).map(i => i ? i.id : i)).to.eql([])
    })
  })
})

