const _ = require('lodash')



const reducedByPatentLife = (steps, timeLeft) => {

  // Keeping track of when we reach a patent-requiring step without patent
  let hasReachedBreak = false

  if (timeLeft == undefined)
    throw 'No Time Left Provided'

  // Add one since we're decreasing before doing work
  timeLeft += 1

  let result = []

  // Go over each possible step in order
  for (const step of steps) {

    // Count down patent
    timeLeft -= 1

    if (step == undefined) {

      // Add empty steps without thinking about it
      result.push(step)


    } else if (step != undefined && step.requires_patent == undefined) {

      // Throw if input is invalid
      throw new Error('requires_patent parameter missing')


    } else if (timeLeft > 0 || !step.requires_patent) {

      // Add if patent life remains or not is not needed
      result.push(step)


    } else {

      // Not allowed to continue beyond first break
      break

    }

  }

  return result
}



module.exports = {
  reducedByPatentLife,
}
