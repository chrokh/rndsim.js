
const interpolation = (prop, stages, stageIndex, prevStage, nextStage, x) => {
  const stage = stages[stageIndex]
  if (stage[prop] == 'linear' && stageIndex != 0 && stageIndex != stages.length - 1) {
    const slope = (nextStage[prop] / nextStage.time - prevStage[prop] / prevStage.time) / (stage.time + 1)
    const offset = prevStage[prop] / prevStage.time
    return x * slope + offset
  } else {
    return stage[prop] / stage.time
  }
}


module.exports = (steps, stages) => {

  var stepChunkIndex = 0
  let result = []

  const numStages = stages.length
  for (var stageIndex = 0; stageIndex < numStages; stageIndex++) {
    const stage = stages[stageIndex]

    const lastStepChunkIndex = stepChunkIndex
    stepChunkIndex += stage.time
    const chunk = steps.slice(lastStepChunkIndex, stepChunkIndex)

    var stepIndex = 0
    while (stepIndex < chunk.length) {
      const step = chunk[stepIndex]
      result.push({
        id:                 step.id,
        stage:              step.stage,
        group:              step.group,
        point_of_interest:  step.point_of_interest,
        requires_patent:    step.requires_patent,
        prob:               step.prob,
        cost: interpolation('cost',
          stages,
          stageIndex,
          stages[stageIndex - 1],
          stages[stageIndex + 1],
          stepIndex + 1),
        cash: interpolation('cash',
          stages,
          stageIndex,
          stages[stageIndex - 1],
          stages[stageIndex + 1],
          stepIndex + 1),
      })
      stepIndex ++
    }
  }

  return result
}
