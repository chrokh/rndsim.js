const evaluate               = require('../utility/evaluate')
const enpv                   = require('../financials/enpv')

const findAllInterventions   = require('../selectors/interventions').all
const timeSpent              = require('../selectors/projects').timeSpent
const stepsPassed            = require('../selectors/projects').stepsPassed
const grantsAvailable        = require('../selectors/projects').grantsAvailable
const selectNestedProject    = require('../selectors/nested').project



module.exports = (state, cache, projectId, discountRate) => {

  if (discountRate == undefined || isNaN(discountRate))
    throw new Error('Cannot evaluate without discount rate')

  // Get or getset nested project from cache
  const project = cache.grab(projectId, () =>
    selectNestedProject(state, projectId))

  const interventions = findAllInterventions(state)
  const passed = stepsPassed(state, projectId)
  const grants = grantsAvailable(state, projectId)
  const patentLife = 20 * 12 // TODO: Un-hard code
  const spent = timeSpent(state, projectId)
  const patentLifeLeft = patentLife - spent

  // Send to valuation
  return evaluate(project, interventions, patentLife, (steps, intervention) => {

    // Generate cache key
    const interventionId = intervention ? intervention.id : ''
    const cacheKey = `${project.id}-${passed}-${interventionId}`

    // Check if exists in cache
    const value = cache.grab(cacheKey, () => {
      const stagesRemaining = steps.slice(passed)
      return enpv(stagesRemaining, discountRate)
    })

    // Add available grants to valuation since it is liquid and free money
    return value + grants

  })

}

