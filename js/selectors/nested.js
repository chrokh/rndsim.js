const denormalize = require('../synthesizers/normalizer').denormalize
const allProjectTypes = require('../selectors/project-types').all

const projectType = (state, id) => {
  const input = { project_types: [id] }
  return denormalize(state, input).project_types[0]
}

const projectTypes = (state) => {
  const ids = allProjectTypes(state).map(t => t.id)
  const input = { project_types: ids }
  return denormalize(state, input).project_types
}

const interventionTypes =  (state) =>
  Object.keys(state.intervention_types).
    map(id => interventionType(state, id))

const interventionType = (state, id) => {
  const input = { intervention_types: [id] }
  return denormalize(state, input).intervention_types[0]
}

const project = (state, id) => {
  const input = { projects: [id] }
  return denormalize(state, input).projects[0]
}

module.exports = {
  projectType,
  projectTypes,
  interventionTypes,
  interventionType,
  project
}
