const normalize   = require('../synthesizers/normalizer').normalize
const denormalize = require('../synthesizers/normalizer').denormalize


const findMany = (state, entity, ids = []) => {
  return ids.map(id => state[entity][id])
}

const all = (state, entity) => {
  return Object.values(state[entity])
}

const isolate = (state, entity, id) => {
  const input = {}
  input[entity] = [id]
  return normalize(denormalize(state, input))
}



module.exports = {
  all,
  findMany,
  isolate,
}
