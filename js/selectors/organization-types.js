const _findMany = require('./generic').findMany
const _of = require('./organizations').type


const all = (state) =>
  Object.values(state.organization_types)

const find = (state, id) =>
  state.organization_types[id]

const findMany = (state, ids) =>
  _findMany(state, 'organization_types', ids)

const potentialPartners = (state, orgId) =>
  findMany(state, of(state, orgId).potential_partners)

const of = (state, orgId) =>
  _of(state, orgId)

const initialSpawns = (state, id) =>
  find(state, id).spawn

const costOfCapital = (state, id) =>
  find(state, id).cost_of_capital

const compounder = (state, id) =>
  find(state, id).compounder


module.exports = {
  all,
  find,
  findMany,
  costOfCapital,
  compounder,
  potentialPartners,
  of,
  initialSpawns
}
