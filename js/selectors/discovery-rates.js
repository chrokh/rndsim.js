const all = (state) =>
  Object.values(state.discovery_rates)

const find = (state, id) =>
  state.discovery_rates[id]

const rate = (state, id) =>
  find(state, id).rate




module.exports = {
  all,
  rate,
}
