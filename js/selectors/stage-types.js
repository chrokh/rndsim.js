const all = (state) => {
  return Object.values(state.stage_types)
}

const find = (state, id) =>
  state.stage_types[id]


module.exports = {
  all,
  find,
}
