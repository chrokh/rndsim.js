const findOrganizationType = require('../selectors/organization-types').find
const findDiscoveryRate    = require('../selectors/discovery-rates').rate
const _findMany            = require('../selectors/generic').findMany


const all = (state) => {
  return Object.values(state.project_types)
}

const find = (state, id) => {
  return state.project_types[id]
}

const findMany = (state, id) =>
  _findMany(state, 'project_types', id)

const totalDiscoveryRate = (state) =>
  all(state).
    map(r => discoveryRate(state, r.id).value).
    reduce((acc, r) => acc + r, 0)

const discoveryRate = (state, id) =>
  findDiscoveryRate(state,
    find(state, id).discovery_rate)

const initialSpawns = (state, id) =>
  find(state, id).spawn

const firstOwner = (state, id) =>
  findOrganizationType(state,
    find(state, id).first_owner)


module.exports = {
  all,
  find,
  findMany,
  totalDiscoveryRate,
  discoveryRate,
  initialSpawns,
  firstOwner,
}
