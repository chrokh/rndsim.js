const _all                 = require('./generic').all
const _findMany            = require('./generic').findMany
const findOrganizationType = require('./organization-types').find


const all = (state) =>
  _all(state, 'organizations')

const find = (state, id) =>
  state.organizations[id]

const findMany = (state, ids) =>
  _findMany(state, 'organizations', ids)

const costOfCapital = (state, id) =>
  find(state, id).cost_of_capital.value

const threshold = (state, id) =>
  find(state, id).threshold.value


const hasProjects = (state, id) =>
  find(state, id).projects.length > 0

const hasInfiniteCapital = (state, id) => {
  return find(state, id).infinite_capital
}

const withInfiniteCapital = (state) =>
  all(state).filter(org => org.infinite_capital)

const type = (state, id) =>
  findOrganizationType(state, find(state, id).type)

const giants = (state) =>
  findMany(state, state.index.giants)

let nextStage
const numberOfProjectsInSameStageAs = (state, id, projectId) => {
  // TODO: Temporary hack. I've put the require here
  // since there's a circular dependency between
  // selectors/organizations AND selectors/projects
  // by proxy
  nextStage = nextStage || require('./stages').nextOrLast

  const stageId = nextStage(state, projectId).id
  return find(state, id).projects.
    map(p => nextStage(state, p)).
    filter(s => s.id == stageId).
    length
}


module.exports = {
  all,
  find,
  giants,
  costOfCapital,
  threshold,
  hasProjects,
  hasInfiniteCapital,
  type,
  numberOfProjectsInSameStageAs,
  withInfiniteCapital,
}
