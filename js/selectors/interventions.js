const _findMany  = require('./generic').findMany
const findStages = require('./stages').findMany

const all = (state) =>
  Object.values(state.interventions)

const find = (state, id) =>
  state.interventions[id]

const findMany = (state, ids) =>
  _findMany(state, ids)

const activeType = (state) => {
  const actives = all(state)
  if (actives.length > 0)
    return actives[0].type
  else
    return 'N/A'
}

const additions = (state) =>
  all(state).filter(i => i.strategy == 'ADD') // TODO: Extract enum

const replacements = (state) =>
  all(state).filter(i => i.strategy == 'REPLACE') // TODO: Extract enum

const totalSize = (state) =>
  all(state).map(i =>
    findStages(state, i.stages).
    map(s => s.cash).
    reduce((sum, n) => sum + n, 0)).
    reduce((sum, n) => sum + n, 0)

const targetProjects = (state, id) =>
  find(state, id).target_projects

let projectGroup
const isEligibleFor = (state, interventionId, projectId) => {
  projectGroup = projectGroup || require('./projects').group
  const group = projectGroup(state, projectId)
  const target = targetProjects(state, interventionId)
  return target.includes(group)
}


module.exports = {
  all,
  find,
  findMany,
  additions,
  replacements,
  totalSize,
  activeType,
  isEligibleFor,
}
