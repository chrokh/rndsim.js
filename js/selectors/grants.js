const currentTick = require('./simulation').currentTick


const all = (state) =>
  Object.values(state.grants)

const find = (state, id) =>
  state.grants[id]

const targetStages = (state, id) =>
  find(state, id).target_stages

const targetProjects = (state, id) =>
  find(state, id).target_projects

const isInCycle = (state, id) =>
  currentTick(state) % find(state, id).cycle == 0

const checkClaim = (state, grantId, claim) => {
  const _remaining = remaining(state, grantId)
  return (claim > _remaining) ? _remaining : claim
}

const totalSize = (state) => {
  let total = 0
  for (const grant of all(state)) {
    total += size(state, grant.id)
  }
  return total
}

const averageFraction = (state) => {
  let total = 0
  let grants = all(state)
  for (const grant of grants) {
    total += fraction(state, grant.id)
  }
  return total / grants.length
}

const size = (state, id) =>
  find(state, id).size

const fraction = (state, id) =>
  find(state, id).fraction

const grantFraction = (state, grantId) =>
  find(state, grantId).fraction

const remaining = (state, grantId) =>
  find(state, grantId).remaining


module.exports = {
  all,
  find,
  size,
  fraction,
  totalSize,
  averageFraction,
  remaining,
  targetStages,
  targetProjects,
  isInCycle,
  checkClaim,
  grantFraction,
}
