const find = (state) =>
  state.simulations[undefined]


const numberOfTicks = (state) =>
  find(state).number_of_ticks

const numberOfRuns = (state) =>
  find(state).number_of_runs


const currentTick = (state) =>
  find(state).tick

const currentRun = (state) =>
  find(state).run


const firstTick = (state) =>
  find(state).first_tick

const firstRun = (state) =>
  find(state).first_run


const lastTick = (state) =>
  find(state).first_tick + find(state).number_of_ticks - 1

const lastRun = (state) =>
  find(state).first_run + find(state).number_of_runs - 1


const cycle = (state) =>
  find(state).cycle

const isAtStartOfCycle = (state) =>
  currentTick(state) % cycle(state) == 0


const seed = (state) =>
  find(state).seed


module.exports = {
  isAtStartOfCycle,
  numberOfTicks,
  numberOfRuns,
  currentTick,
  currentRun,
  firstTick,
  firstRun,
  lastTick,
  lastRun,
  seed,
}
