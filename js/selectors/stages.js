const _findMany         = require('./generic').findMany



const find = (state, id) => {
  return state.stages[id]
}

const findMany = (state, ids) => {
  return _findMany(state, 'stages', ids)
}

const hasReceivedGrant = (state, id) =>
  find(state, id).has_received_grant



module.exports = {
  find,
  findMany,
  hasReceivedGrant,
}
