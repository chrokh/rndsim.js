const all = (state) =>
  Object.values(state.investors)

const find = (state, id) =>
  state.investors[id]

const investorsInvestingIn = (state, stageGroup) =>
  all(state).filter(i => i.targets.includes(stageGroup))

const costOfCapital = (state, id) =>
  find(state, id).cost_of_capital

const compounder = (state, id) =>
  find(state, id).compounder

const fraction = (state, id) =>
  find(state, id).fraction

const threshold = (state, id) =>
  find(state, id).threshold



module.exports = {
  all,
  find,
  fraction,
  threshold,
  compounder,
  costOfCapital,
  investorsInvestingIn,
}
