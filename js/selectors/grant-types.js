const all = (state) =>
  Object.values(state.grant_types)

module.exports = {
  all,
}
