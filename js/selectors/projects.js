const grantProjectTargets   = require('../selectors/grants').targetProjects
const grantStageTargets     = require('../selectors/grants').targetStages
const hasInfiniteCapital    = require('../selectors/organizations').hasInfiniteCapital
const findOrganization      = require('../selectors/organizations').find
const findInvestor          = require('../selectors/investors').find
const findStage             = require('../selectors/stages').find
const stageHasReceivedGrant = require('../selectors/stages').hasReceivedGrant
const discoveryRateOfType  = require('../selectors/project-types').discoveryRate

const States      = require('../constants/project-states')
const currentTick = require('./simulation').currentTick

const max          = require('../math/combinators').max
const mean         = require('../math/combinators').equally

const _all         = require('./generic').all
const _findMany    = require('./generic').findMany


const all = (state) => {
  return _all(state, 'projects')
}

const find = (state, id) => {
  return state.projects[id]
}

const findMany = (state, ids) => {
  return _findMany(state, 'projects', ids)
}

const owner = (state, id) =>
  findOrganization(state, find(state,id).owner)

const hibernating = (state) => {
  return all(state).filter(p => p.state === States.HIBERNATING)
}

const initializing = (state) =>
  all(state).filter(p => p.state === States.INITIALIZING)

const developing = (state) =>
  all(state).filter(p => p.state == States.DEVELOPING)

const deciding = (state) =>
  all(state).filter(p => p.state === States.DECIDING)

const timeSpentInHibernation = (state, id) => {
  if (isHibernating(state, id)) {
    return currentTick(state) - enteredHibernationAt(state, id)
  } else {
    return 0
  }
}

const maxHibernationTime = (state, id) =>
  find(state, id).max_hibernation

const enteredHibernationAt = (state, id) =>
  find(state, id).entered_hibernation_at

const state = (_state, id) =>
  find(_state, id).state

const isHibernating = (state, id) =>
  find(state, id).state === States.HIBERNATING

const isDeveloping = (state, id) =>
  find(state, id).state === States.DEVELOPING

const isAtDecisionPoint = (state, id) => {
  const last = prevStep(state, id)
  const next = nextStep(state, id)
  if (last != undefined)
    prevStep(state, id).stage != nextStep(state, id).stage
  else
    return true
}

const isAtPointOfInterest = (state, id) =>
  nextStep(state, id).point_of_interest

const isEligibleForInvestment = (state, id) =>
  isHibernating(state, id)


const isEligibleForPartnership = (state, id) =>
  isHibernating(state, id) &&
    !hasPartner(state, id)

const isEligibleForExit = (state, id) =>
  isHibernating(state, id)

const isEligibleForGrant = (state, id, grantId) =>
  (isHibernating(state, id) || isDeveloping(state, id)) &&
    grantStageTargets(state, grantId).includes(nextStepGroup(state, id)) &&
    grantProjectTargets(state, grantId).includes(group(state, id)) &&
    !nextStageHasReceivedGrant(state, id)

const eligibleForGrant = (state, grantId) =>
  all(state).filter(p => isEligibleForGrant(state, p.id, grantId))

const eligibleForPartnership = (state) =>
  all(state).filter(p => isEligibleForPartnership(state, p.id))

const eligibleForInvestment = (state) =>
  all(state).filter(p => isEligibleForInvestment(state, p.id))

const eligibleForExit = (state) =>
  all(state).filter(p => isEligibleForExit(state, p.id))

const hasPartner = (state, id) =>
  find(state, id).partners.length > 0

const grantsAccumulated = (state, id) =>
  find(state, id).grants_accumulated

const grantsSpent = (state, id) =>
  find(state, id).grants_spent

const capitalAccumulated = (state, id) =>
  find(state, id).capital_accumulated

const capitalSpent = (state, id) =>
  find(state, id).capital_spent

const capitalAvailable = (state, id) =>
  capitalAccumulated(state, id) - capitalSpent(state, id)

const grantsAvailable = (state, id) =>
  grantsAccumulated(state, id) - grantsSpent(state, id)

const cash = (state, id) => {
  if (!hasAccessToInfiniteCapital(state, id))
    return capitalAvailable(state, id) + grantsAvailable(state, id)
}

const affordsNextStep = (state, id) =>
  hasAccessToInfiniteCapital(state, id) || hasEnoughCapitalForNextStep(state, id)

const hasAccessToInfiniteCapital = (state, id) =>
  hasInfiniteCapital(state, owner(state, id).id) ||
    hasPartnerWithInfiniteCapital(state, id)

const hasPartnerWithInfiniteCapital = (state, id) =>
  find(state, id).partners.reduce((acc, p) => acc ||
    hasInfiniteCapital(state, p), false)

const hasEnoughCapitalForNextStep = (state, id) =>
  nextStepCost(state, id) <= cash(state, id)

const partners = (state, id) =>
  find(state, id).partners.
    map(p => findOrganization(state, p))

const investments = (state, id) =>
  find(state, id).investments

const investors = (state, id) =>
  investments(state, id).
    map(investment => findInvestor(state, investment.investor))

const investorIds = (state, id) =>
  investments(state, id).
    map(investment => investment.investor)

// TODO: Refactor p.threshold.value to instead use a method on a wrapped org
const compositeThreshold = (state, id) => {
  const _partners  = partners(state, id).map(p => p.threshold.value)
  const _investors = investors(state, id).map(p => p.threshold.value)
  const _owner     = owner(state, id).threshold.value
  return mean([..._partners, max([..._investors, _owner])])
}

// TODO: Refactor p.cost_of_capital.value to instead use a method on a wrapped org
const compositeCostOfCapital = (state, id) => {
  const _partners  = partners(state, id).map(p => p.cost_of_capital.value)
  const _investors = investors(state, id).map(p => p.cost_of_capital.value)
  const _owner     = owner(state, id).cost_of_capital.value
  return mean([..._partners, max([..._investors, _owner])])
}

const timeSpent = (state, id) =>
  timeSpentBeforeSimulation(state, id) +
    timeSpentInSimulation(state, id)

const timeSpentBeforeSimulation = (state, id) =>
  find(state, id).initial_time_passed

const timeSpentInSimulation = (state, id) =>
  currentTick(state) + 1 - find(state, id).time_of_discovery

const valuation = (state, id) =>
  find(state, id).valuation

const discoveryRate = (state, id) =>
  discoveryRateOfType(state, find(state, id).type).value

const source = (state, id) =>
  find(state, id).source

const group = (state, id) =>
  find(state, id).group

const nextStage = (state, id) =>
  findStage(state, nextStep(state, id).stage)

const nextStageHasReceivedGrant = (state, id) =>
  stageHasReceivedGrant(state, nextStage(state, id).id)

const remainingSteps = (state, id) =>
  find(state, id).steps.slice(stepsPassed(state, id))

const remainingStageCost = (state, id) => {
  let cost = 0
  let stage = undefined
  for (const step of remainingSteps(state, id)) {
    if (stage == undefined) stage = step.stage
    if (stage != step.stage) return cost
    cost += step.cost
  }
}

const nextStageHasReceivedGrants = (state, id) => 
  nextStage(state, id).has_received_grant

const nextStepGroup = (state, id) =>
  nextStep(state, id).group

const nextStepCost = (state, id) =>
  nextStep(state, id).cost

const nextStepProb = (state, id) =>
  nextStep(state, id).prob

const nextStep = (state, id) => {
  const next = find(state, id).steps[find(state, id).steps_passed]
  if (next)
    return next
  else
    return {
      group:              'N/A',
      point_of_interest:  true,
      cost:               0,
    }
}

const hasNextStep = (state, id) =>
  stepsPassed(state, id) < steps(state, id).length

const prevStep = (state, id) =>
  find(state, id).steps[find(state, id).steps_passed - 1]

const stepsPassed = (state, id) =>
  find(state, id).steps_passed

const steps = (state, id) =>
  find(state, id).steps

const nextStepHasReceivedGrants = (state, id) =>
  nextStep(state, id).has_received_grants



module.exports = {
  all,
  find,
  findMany,
  owner,
  hibernating,
  timeSpentInHibernation,
  maxHibernationTime,
  enteredHibernationAt,

  nextStage,
  nextStageHasReceivedGrants,

  remainingSteps,
  remainingStageCost,

  hasNextStep,
  nextStep,
  nextStepCost,
  nextStepProb,
  nextStepGroup,

  stepsPassed,

  state,
  developing,
  deciding,
  initializing,
  isHibernating,
  isDeveloping,
  isAtDecisionPoint,
  isAtPointOfInterest,
  isEligibleForInvestment,
  isEligibleForPartnership,
  isEligibleForExit,
  isEligibleForGrant,
  eligibleForGrant,
  eligibleForPartnership,
  eligibleForInvestment,
  eligibleForExit,
  hasPartner,
  grantsAccumulated,
  cash,
  grantsSpent,
  capitalAccumulated,
  capitalSpent,
  capitalAvailable,
  grantsAvailable,
  affordsNextStep,
  hasAccessToInfiniteCapital,
  hasPartnerWithInfiniteCapital,
  hasEnoughCapitalForNextStep,
  partners,
  investorIds,
  investments,
  compositeThreshold,
  compositeCostOfCapital,
  timeSpent,
  valuation,
  discoveryRate,
  source,
  group,
}
