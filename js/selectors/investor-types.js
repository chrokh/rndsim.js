const nextStep = require('./projects').nextStep


const all = (state) =>
  Object.values(state.investor_types)

const find = (state, id) =>
  state.investor_types[id]

const costOfCapital = (state, id) =>
  find(state, id).cost_of_capital

const compounder = (state, id) =>
  find(state, id).compounder

const fraction = (state, id) =>
  find(state, id).fraction

const spawn = (state, id) =>
  find(state, id).spawn



module.exports = {
  all,
  find,
  costOfCapital,
  compounder,
  fraction,
  spawn,
}
