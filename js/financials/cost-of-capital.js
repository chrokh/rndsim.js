const recalculate = (rate, compounder) =>
  Math.pow((1 + rate), compounder) - 1


module.exports = {
  recalculate
}
