const expect = require('chai').expect
const recalculate = require('./cost-of-capital').recalculate


describe('CostOfCapital.recalculate', () => {

  const yearly     = 0.11
  const monthly    = 0.008734594

  it('can recalculate YEARLY to MONTHLY', () => {
    const compounder = 1/12
    expect(recalculate(yearly, compounder).toFixed(7)).
      to.equal(monthly.toFixed(7))
  })

  it('can recalculate MONTHLY to YEARLY', () => {
    const compounder = 12
    expect(recalculate(monthly, compounder).toFixed(7)).
      to.equal(yearly.toFixed(7))
  })

})
