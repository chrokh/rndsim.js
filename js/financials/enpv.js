const Finance = require('financejs');
const finance = new Finance();


module.exports = (periods, discount) => {

  // Default to 0 if not given periods
  if (!periods || periods.length <= 0) return 0

  // Calculate up front cost
  const initial = periods[0].cash - periods[0].cost

  // Prepare for aggregating probability
  let prob = 1 * periods[0].prob

  // Prepare for aggregating cashflows
  let cashflows = []

  // Cache the number of periods
  var numPeriods = periods.length

  for (var i = 1; i < numPeriods; i++) {
    var cashflow = periods[i]

    // Add cashflow
    cashflows.push(cashflow.cash - cashflow.cost)

    // Add probability
    prob *= cashflow.prob
  }

  // ==> NPV + probability + up front cashflows
  // Lib expects discount rate as percentage, thus * 100
  return finance.NPV(discount * 100, 0, ...cashflows) * prob + initial
}

