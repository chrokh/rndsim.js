const expect = require('chai').expect
const enpv   = require('./enpv')



describe('Financials.enpv', () => {
  const cashflows = [
    { cash: 0,        cost: 1.08,  prob: 0.69,  },
    { cash: 0,        cost: 7.4,   prob: 0.84,  },
    { cash: 0,        cost: 7.1,   prob: 0.74,  },
    { cash: 0,        cost: 26.9,  prob: 0.79,  },
    { cash: 0,        cost: 1.96,  prob: 0.99,  },
    { cash: 63.4,     cost: 0,     prob: 1,     },
    { cash: 11.02,    cost: 0,     prob: 1,     },
    { cash: 199.87,   cost: 0,     prob: 1,     },
    { cash: 327.17,   cost: 0,     prob: 1,     },
    { cash: 497.66,   cost: 0,     prob: 1,     },
    { cash: 735.55,   cost: 0,     prob: 1,     },
    { cash: 955.58,   cost: 0,     prob: 1,     },
    { cash: 1082.88,  cost: 0,     prob: 1,     },
    { cash: 1284.48,  cost: 0,     prob: 1,     },
    { cash: 1559,     cost: 0,     prob: 1,     },
  ]

  it('is calculated correctly', () => {
    const rate = 0.02

    const go = (x, y) => enpv(cashflows.slice(x, y + 1), rate)

    var actual = [
      [go(0,0),    go(0,1),    go(0,2),    go(0,3),    go(0,4),    go(0,5),   go(0,6),   go(0,7),   go(0,8),   go(0,9),   go(0,10),  go(0,11),  go(0,12),  go(0,13),  go(0,14)],
      [go(1,1),    go(1,2),    go(1,3),    go(1,4),    go(1,5),    go(1,6),   go(1,7),   go(1,8),   go(1,9),   go(1,10),  go(1,11),  go(1,12),  go(1,13),  go(1,14)],
      [go(2,2),    go(2,3),    go(2,4),    go(2,5),    go(2,6),    go(2,7),   go(2,8),   go(2,9),   go(2,10),  go(2,11),  go(2,12),  go(2,13),  go(2,14)],
      [go(3,3),    go(3,4),    go(3,5),    go(3,6),    go(3,7),    go(3,8),   go(3,9),   go(3,10),  go(3,11),  go(3,12),  go(3,13),  go(3,14)],
      [go(4,4),    go(4,5),    go(4,6),    go(4,7),    go(4,8),    go(4,9),   go(4,10),  go(4,11),  go(4,12),  go(4,13),  go(4,14)],
      [go(5,5),    go(5,6),    go(5,7),    go(5,8),    go(5,9),    go(5,10),  go(5,11),  go(5,12),  go(5,13),  go(5,14)],
      [go(6,6),    go(6,7),    go(6,8),    go(6,9),    go(6,10),   go(6,11),  go(6,12),  go(6,13),  go(6,14)],
      [go(7,7),    go(7,8),    go(7,9),    go(7,10),   go(7,11),   go(7,12),  go(7,13),  go(7,14)],
      [go(8,8),    go(8,9),    go(8,10),   go(8,11),   go(8,12),   go(8,13),  go(8,14)],
      [go(9,9),    go(9,10),   go(9,11),   go(9,12),   go(9,13),   go(9,14)],
      [go(10,10),  go(10,11),  go(10,12),  go(10,13),  go(10,14)],
      [go(11,11),  go(11,12),  go(11,13),  go(11,14)],
      [go(12,12),  go(12,13),  go(12,14)],
      [go(13,13),  go(13,14)],
      [go(14,14)],
    ]

    // Test case based on calculations in file: ENPV_EX02_v03.xlsx
    var expected = [
      [-1.080000,-5.284941,-7.118625,-14.439442,-14.913252,4.349165,7.631652,65.998843,159.667541,299.353763,501.764182,759.566931,1045.985160,1379.064347,1775.402693],
      [-7.400000,-11.726824,-23.514866,-24.251620,4.223258,9.075629,95.357564,233.824334,440.317010,739.532413,1120.632129,1544.032988,2036.410917,2622.302385],
      [-7.100000,-22.517392,-23.453528,11.123109,17.015274,121.786195,289.924416,540.665523,903.998512,1366.762453,1880.892068,2478.779553,3190.219193],
      [-26.900000,-28.402859,19.256830,27.378463,171.792434,403.550523,749.166643,1249.976979,1887.840790,2596.505935,3420.621117,4401.254134],
      [-1.960000,59.575294,70.061453,256.519999,555.751962,1001.990496,1648.606373,2472.177370,3387.162746,4451.210196,5717.343966],
      [63.400000,74.203922,266.312726,574.612324,1034.373239,1700.583536,2549.111229,3491.823436,4588.114748,5892.616207],
      [11.020000,206.970980,521.436571,990.392704,1669.927207,2535.425454,3496.991904,4615.209042,5945.800531],
      [199.870000,520.624902,998.960158,1692.085351,2574.893563,3555.691342,4696.272823,6053.476142],
      [327.170000,815.071961,1522.059658,2422.524034,3422.937769,4586.330880,5970.678265],
      [497.660000,1218.787451,2137.261115,3157.683125,4344.344097,5756.378430],
      [735.550000,1672.393137,2713.223587,3923.617779,5363.892799],
      [955.580000,2017.227059,3251.829135,4720.909655],
      [1082.880000,2342.174118,3840.636248],
      [1284.480000,2812.911373],
      [1559.000000],
    ]

    // TODO: Rounding errors even with a single decimal
    // But this seems to be due to Javascript's lack of
    // rounding facilities rather than the algorithm.
    const round = (n) => n.toFixed(0)

    actual   = actual.map(arr => arr.map(n => round(n)))
    expected = expected.map(arr => arr.map(n => round(n)))

    expect(actual).to.eql(expected)
  })




  it('returns 0 when given no periods', () => {
    expect(enpv([])).to.eq(0)
    expect(enpv(undefined)).to.eq(0)
  })

})

