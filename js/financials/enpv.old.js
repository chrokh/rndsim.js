const enpv = (periods, discount) => {
  if (!periods || periods.length <= 0) return 0
  periods = _addParameters(periods)
  const _ = {} // cache!
  var sum = 0
  for (var i=0; i < periods.length; i++) {
    sum += rnpv(i, discount, periods, _)
  }
  return sum
}

const rnpv = (index, discount, periods, _) => {
  return presentValue(
    riskAdjustedCashflow(index, periods, _),
    discount,
    periods[index].startTime)
}

const presentValue = (cashflow, discount, t) => {
  return cashflow / Math.pow(1 + discount, t)
}

const riskAdjustedCashflow = (index, periods, _) => {
  const cashflow = periods[index].cash - periods[index].cost
  return (cashflow * totalProbability(periods, _)) / remainingProbability(periods, index, _)
}

const totalProbability = (periods, _) => {
  if (_.totalProbability == null) {
    _.totalProbability = probabilityProduct(periods, 0)
  }
  return _.totalProbability
}

const remainingProbability = (periods, index) =>
  probabilityProduct(periods, index)

const probabilityProduct = (periods, index) => {
  var product = 1
  for (var i=index; i < periods.length; i++) {
    product *= periods[i].prob
  }
  return product
}


const _addParameters = (periods) => {
  var t = 0
  for (const period of periods) {
    period.startTime = t++
    period.cashflow = period.cash - period.cost
  }
  return periods
}


// Alternative calculation
// (rpvs can also be calculated as the sum of rpv's)
// Last time I measured, this one was slower, but
// that was before translating the other calculation
// to a for-style rather than reduce-style.
//const rpv = (period, discount, periods, _) => {
//  const t = period.startTime
//  const cashflow    = period.cash - period.cost
//  const nominator   = cashflow * totalProbability(periods, _)
//  const denominator = remainingProbability(periods, period) * Math.pow((1 + discount), t)
//  return nominator / denominator
//}



module.exports = {
  enpv
}
