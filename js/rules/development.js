const incrementStep           = require('../actions/projects').incrementStep
const payForNextStep          = require('../actions/projects').payForNextStep

const Transitions             = require('../actions/transitions')

const isAtPointOfInterest     = require('../selectors/projects').isAtPointOfInterest
const nextStepProb            = require('../selectors/projects').nextStepProb



module.exports = (store, randomizer, projectId) => {

  // Pay first..
  store.dispatch(payForNextStep, projectId)

  // ..Try later
  if (randomizer.flip(store.select(nextStepProb, projectId))) {

    // Success!
    store.dispatch(incrementStep, projectId)

    // Reached point of interest?
    if (store.select(isAtPointOfInterest, projectId)) {
      store.dispatch(Transitions.markAsCompleted, projectId)
    } else {
      store.dispatch(Transitions.markAsInitializing, projectId)
    }

  } else {
    // Failure!
    store.dispatch(Transitions.markAsFailed, projectId)
  }
}
