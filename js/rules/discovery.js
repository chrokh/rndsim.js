const Sources                = require('../constants/project-sources')
const build                  = require('../builders/project')

const researchers            = require('../selectors/researchers').all
const projectTypes           = require('../selectors/project-types').findMany
const projectType            = require('../selectors/nested').projectType
const currentTick            = require('../selectors/simulation').currentTick
const discoveryRate          = require('../selectors/project-types').discoveryRate

const add                    = require('../actions/generic').addNested
const addProjectToResearcher = require('../actions/projects').addProjectToResearcher



module.exports = function (store, randomizer) {

  // For every researcher
  for (const researcher of store.select(researchers)) {

    // Researchers can only discover if they are still active
    if (store.select(currentTick) <= (researcher.max_tick || Infinity)) {

      // For every project type that researcher can discover
      for (const type of store.select(projectTypes, researcher.discoverables)) {

        // How many did they discover this time?
        const numberOfDiscoveries = randomizer.multiflip(
          store.select(discoveryRate, type.id).value)

        // Spawn one per discovery
        for (let i = 0; i < numberOfDiscoveries; i++) {

          // Find project spec
          const spec = store.cacheGrab(`ptype${type.id}`, () =>
            store.select(projectType, type.id))

          // Add info about when it was discovered
          const extended = Object.assign({}, spec, {

            // When it was discovered
            timeOfDiscovery: store.select(currentTick),

            // That it was not part of initial state
            source: Sources.ORGANIC

          })

          // Build project from spec
          const project = build(extended, randomizer)

          // Save project
          store.dispatch(add, 'projects', project)
          store.dispatch(addProjectToResearcher, project.id, researcher.id)

        }

      }

    }
  }
}
