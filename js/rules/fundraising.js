const Partnering              = require('./partnering')
const Exiting                 = require('./exiting')
const Investing               = require('./investing')

const timeSpentInHibernation  = require('../selectors/projects').timeSpentInHibernation
const maxHibernationTime      = require('../selectors/projects').maxHibernationTime
const affordsNextStep         = require('../selectors/projects').affordsNextStep

const Transitions             = require('../actions/transitions')
const isPowerOfTwo            = require('../math/general').isPowerOfTwo


const alternatives = [
  Partnering,
  Exiting,
  Investing
]

module.exports = (store, randomizer, projId) => {

  const timeHibernated = store.select(timeSpentInHibernation, projId)


  // Has hibernated too long?
  if (timeHibernated > store.select(maxHibernationTime, projId)) {

    // Mark as exhausted
    store.dispatch(Transitions.markAsExhausted, projId)

  }


  // Has hibernated long enough?
  else if (isPowerOfTwo(timeHibernated) || isPowerOfTwo(timeHibernated + 1)) {

    // Choose and run alternative
    randomizer.pick(
      alternatives.filter(alt => alt.isAvailableTo(store, projId))).
      run(store, randomizer, projId)

    // Secured enough capital?
    if (store.select(affordsNextStep, projId)) {
      store.dispatch(Transitions.markAsInitializing, projId)
    }

  }


}
