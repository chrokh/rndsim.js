const recalculate       = require('../financials/cost-of-capital').recalculate
const updateRecord      = require('../actions/generic').updateRecord
const transform         = require('../calculations/distribution').transform

const organizationTypes         = require('../selectors/organization-types').all
const organizationCostOfCapital = require('../selectors/organization-types').costOfCapital
const organizationCompounder    = require('../selectors/organization-types').compounder

const investorTypes          = require('../selectors/investor-types').all
const investorCostOfCapital  = require('../selectors/investor-types').costOfCapital
const investorCompounder     = require('../selectors/investor-types').compounder


module.exports = (store, randomizer) => {


  /*
   * Organization types
   */

  for (const type of store.select(organizationTypes)) {

    const compounder    = store.select(organizationCompounder,    type.id)
    const costOfCapital = store.select(organizationCostOfCapital, type.id)

    const f = (x) => recalculate(x, compounder)

    store.dispatch(updateRecord, 'organization_types', type.id, (entity) => ({
      cost_of_capital: transform(costOfCapital, f)
    }))

  }




  /*
   * Investor types
   */

  for (const type of store.select(investorTypes)) {

    const compounder    = store.select(investorCompounder,    type.id)
    const costOfCapital = store.select(investorCostOfCapital, type.id)

    const f = (x) => recalculate(x, compounder)

    store.dispatch(updateRecord, 'investor_types', type.id, (entity) => ({
      cost_of_capital: transform(costOfCapital, f)
    }))

  }


}
