const all = require('../selectors/investor-types').all
const spawn = require('../selectors/investor-types').spawn
const add  = require('../actions/investors').add
const build = require('../builders/investor')
const currentRun = require('../selectors/simulation').currentRun

module.exports = (store, randomizer) => {

  for (const spec of store.select(all)) {

    // How many?
    let count = store.select(spawn, spec.id)

    // TODO: THIS IS A TEMPORARY HACK
    const run = store.select(currentRun)

    for (let i = count; i > 0; i--) {
      // TODO: PART OF SAME HACK AS ABOVE
      const extraSpec = Object.assign({ multiplier: run % 2 != 0 ? 1 : 0 }, spec)
      store.dispatch(add, build(extraSpec, randomizer))
    }
  }

}
