const transferOwnership = require('../actions/projects').transfer
const evaluate          = require('../queries/evaluate')

const isEligibleForExit = require('../selectors/projects').isEligibleForExit
const owner             = require('../selectors/projects').owner

const giants            = require('../selectors/organizations').giants
const threshold         = require('../selectors/organizations').threshold
const costOfCapital     = require('../selectors/organizations').costOfCapital



const run = (store, randomizer, projectId) => {

  const buyers = randomizer.shuffle(store.select(giants))

  for (const buyer of buyers) {

    if (buyer != null) {
      const buyerDiscountRate = store.select(costOfCapital, buyer.id)
      const buyerThreshold    = store.select(threshold, buyer.id)
      const projectValue      = store.query(evaluate, projectId, buyerDiscountRate)

      if (projectValue >= buyerThreshold) {
        const seller = store.select(owner, projectId)
        store.dispatch(transferOwnership, projectId, seller.id, buyer.id)
      }
    }
  }

}


const isAvailableTo = (store, projectId) =>
  store.select(isEligibleForExit, projectId)


module.exports = {
  run,
  isAvailableTo
}
