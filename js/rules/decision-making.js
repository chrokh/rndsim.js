const evaluate = require('../queries/evaluate')

const markAsTerminated = require('../actions/transitions').markAsTerminated
const markAsDeveloping = require('../actions/transitions').markAsDeveloping

const deciding               = require('../selectors/projects').deciding
const compositeThreshold     = require('../selectors/projects').compositeThreshold
const compositeCostOfCapital = require('../selectors/projects').compositeCostOfCapital




module.exports = (store, randomizer, projectId) => {

  const discountRate = store.select(compositeCostOfCapital, projectId)
  const value        = store.select(evaluate, store.cache, projectId, discountRate)
  const threshold    = store.select(compositeThreshold, projectId)

  if (value < threshold) {
    store.dispatch(markAsTerminated, projectId)
  } else {
    store.dispatch(markAsDeveloping, projectId)
  }

}
