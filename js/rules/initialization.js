const affordsNextStep   = require('../selectors/projects').affordsNextStep
const isAtDecisionPoint = require('../selectors/projects').isAtDecisionPoint
const initializing      = require('../selectors/projects').initializing
const nextStep          = require('../selectors/projects').nextStep
const hasNextStep       = require('../selectors/projects').hasNextStep

const markAsHibernating = require('../actions/transitions').markAsHibernating
const markAsDeciding    = require('../actions/transitions').markAsDeciding
const markAsDeveloping  = require('../actions/transitions').markAsDeveloping
const markAsCompleted   = require('../actions/transitions').markAsCompleted


module.exports = (store, randomizer) => {

  // For every project in INITIALIZATION
  for (const proj of store.select(initializing)) {


    if (!store.select(affordsNextStep, proj.id)) {

      // Hibernating, because cannot afford next step
      store.dispatch(markAsHibernating, proj.id)

    } else if (store.select(isAtDecisionPoint, proj.id)) {

      // Deciding, because at decision point
      store.dispatch(markAsDeciding, proj.id)

    } else {

      // Developing, becuase nothing else stands in the way
      store.dispatch(markAsDeveloping, proj.id)

    }


  }

}
