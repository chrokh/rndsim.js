const stageTypes   = require('../selectors/stage-types').all
const updateRecord = require('../actions/generic').updateRecord
const transform    = require('../calculations/distribution').transform

module.exports = (store, randomizer) => {

  for (const type of store.select(stageTypes)) {

    const f = (x) => x * type.correction

    if (type.correction != 1) {
      store.dispatch(updateRecord, 'stage_types', type.id, (entity) => ({
        cash: transform(entity.cash, f),
        cost: transform(entity.cost, f)
      }))
    }

  }

}
