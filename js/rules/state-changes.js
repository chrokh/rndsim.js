const States = require('../constants/project-states')

const fundraising    = require('./fundraising')
const development    = require('./development')
const decisionMaking = require('./decision-making')

const removeProject = require('../actions/projects').remove

const projects         = require('../selectors/projects').all
const state            = require('../selectors/projects').state
const isAtStartOfCycle = require('../selectors/simulation').isAtStartOfCycle


module.exports = (store, randomizer) => {

  const purge = store.select(isAtStartOfCycle)

  // Go through every project in random order
  for (const proj of randomizer.shuffle(store.select(projects))) {

    // Switch over project state
    switch (store.select(state, proj.id)) {

      case States.HIBERNATING:
        fundraising(store, randomizer, proj.id)
        break

      case States.DEVELOPING:
        development(store, randomizer, proj.id)
        break

      case States.DECIDING:
        decisionMaking(store, randomizer, proj.id)
        break

      case States.TERMINATED:
        if (purge)
          store.dispatch(removeProject, proj.id)
        break

      case States.FAILED:
        if (purge)
          store.dispatch(removeProject, proj.id)
        break

      case States.COMPLETED:
        if (purge)
          store.dispatch(removeProject, proj.id)
        break

      case States.EXHAUSTED:
        if (purge)
          store.dispatch(removeProject, proj.id)
        break

    }

  }

}
