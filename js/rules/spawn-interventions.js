const interventionTypes = require('../selectors/nested').interventionTypes
const build             = require('../builders/interventions')
const add               = require('../actions/interventions').add


module.exports = (store, randomizer) => {

  // Pick random intervention
  const spec = randomizer.pick(store.select(interventionTypes))

  // Build random intervention and add it to the store
  if (spec) {
    store.dispatch(add, build(spec, randomizer))
  }

}
