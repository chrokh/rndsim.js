const evaluate                = require('../queries/evaluate')

const isEligibleForInvestment = require('../selectors/projects').isEligibleForInvestment
const remainingStageCost      = require('../selectors/projects').remainingStageCost
const nextStep                = require('../selectors/projects').nextStep
const investorsInvestingIn    = require('../selectors/investors').investorsInvestingIn
const investorFraction        = require('../selectors/investors').fraction
const investorCostOfCapital   = require('../selectors/investors').costOfCapital
const investorThreshold       = require('../selectors/investors').threshold

const associateInvestor       = require('../actions/projects').associateInvestor
const provideCapital          = require('../actions/projects').provideCapital
const add                     = require('../actions/investors').add



const run = (store, randomizer, projectId) => {

  const nextStepGroup = store.select(nextStep, projectId).group
  const investors = store.select(investorsInvestingIn, nextStepGroup)
  const investor = randomizer.pick(investors)

  if (investor != null) {

    const invDiscountRate = store.select(investorCostOfCapital, investor.id)
    const invThreshold = store.select(investorThreshold, investor.id)

    const projectValue = store.query(evaluate, projectId, invDiscountRate)

    if (projectValue >= invThreshold) {

      const claim    = store.select(remainingStageCost, projectId)
      const fracDist = store.select(investorFraction, investor.id)
      const frac     = randomizer.sample(fracDist).value
      const amount   = claim * frac

      store.dispatch(associateInvestor, projectId, investor.id, nextStepGroup)
      store.dispatch(provideCapital, projectId, amount)
    }
  }
}

const isAvailableTo = (store, projectId) =>
  store.select(isEligibleForInvestment, projectId)


module.exports = {
  run,
  isAvailableTo
}
