const discoveryRates = require('../selectors/discovery-rates').all
const discoveryRate  = require('../selectors/discovery-rates').rate
const updateRecord   = require('../actions/generic').updateRecord

module.exports = (store, randomizer) => {

  for (const type of store.select(discoveryRates)) {

    store.dispatch(updateRecord, 'discovery_rates', type.id, () => ({
      rate: randomizer.sample(store.select(discoveryRate, type.id))
    }))

  }

}
