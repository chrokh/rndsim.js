const associatePartner            = require('../actions/projects').associatePartner

const evaluate                    = require('../queries/evaluate')

const isEligibleForPartnership    = require('../selectors/projects').isEligibleForPartnership
const giants                      = require('../selectors/organizations').giants
const threshold                   = require('../selectors/organizations').threshold
const costOfCapital               = require('../selectors/organizations').costOfCapital


const run = (store, randomizer, projectId) => {

  const partner = randomizer.pick(store.select(giants))

  if (partner != null) {
    const partnerDiscountRate = store.select(costOfCapital, partner.id)
    const partnerThreshold    = store.select(threshold, partner.id)
    const projectValue        = store.query(evaluate, projectId, partnerDiscountRate)

    if (projectValue >= partnerThreshold) {
      store.dispatch(associatePartner, projectId, partner.id)
    }
  }

}


const isAvailableTo = (store, projectId) =>
  store.select(isEligibleForPartnership, projectId)


module.exports = {
  run,
  isAvailableTo
}
