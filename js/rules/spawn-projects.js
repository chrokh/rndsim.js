const _                        = require('lodash')
const Sources                  = require('../constants/project-sources')
const addProject               = require('../actions/projects').add
const addOrganization          = require('../actions/organizations').add
const buildProject             = require('../builders/project')
const buildOrganization        = require('../builders/organizations-from-types')
const projectTypes             = require('../selectors/nested').projectTypes
const organizationType         = require('../selectors/organization-types').find
const initialSpawns            = require('../selectors/project-types').initialSpawns
const firstOwner               = require('../selectors/project-types').firstOwner
const addProjectToOrganization = require('../actions/projects').addToOrganization
const currentTick              = require('../selectors/simulation').currentTick

module.exports = (store, randomizer) => {

  for (const projectSpec of store.select(projectTypes)) {

    // Spawn as many as defined by spec
    _(store.select(initialSpawns, projectSpec.id)).times(() => {


      // Build organization
      const organization = buildOrganization(
        // Find org spec of project's first owner
        store.select(firstOwner, projectSpec.id),
        randomizer)

      // Save organization
      store.dispatch(addOrganization, organization)


      // Build project
      const project = buildProject(
        // Add additional info to project spec
        Object.assign({}, projectSpec, {
          source:          Sources.INITIAL,
          timeOfDiscovery: store.select(currentTick)
        }), randomizer)

      // Save project
      store.dispatch(addProject, project)


      // Associate project and organization
      store.dispatch(addProjectToOrganization, project.id, organization.id)

    })

  }

}
