const _                         = require('lodash')

const eligibleForGrant          = require('../selectors/projects').eligibleForGrant
const isHibernating             = require('../selectors/projects').isHibernating
const affordsNextStep           = require('../selectors/projects').affordsNextStep
const remainingStageCost        = require('../selectors/projects').remainingStageCost
const nextStage                 = require('../selectors/projects').nextStage
const grants                    = require('../selectors/grants').all
const isInCycle                 = require('../selectors/grants').isInCycle
const remainingOfGrant          = require('../selectors/grants').remaining
const checkClaim                = require('../selectors/grants').checkClaim
const grantFraction             = require('../selectors/grants').grantFraction

const markAsInitializing        = require('../actions/transitions').markAsInitializing
const provideGrant              = require('../actions/projects').provideGrant
const resetGrant                = require('../actions/grants').reset
const reduceRemainingGrant      = require('../actions/grants').reduceRemaining
const markAsHavingReceivedGrant = require('../actions/stages').markAsHavingReceivedGrant



module.exports = (store, randomizer) => {


  // For every grant...
  for (const grant of store.select(grants)) {


    // Renew grant if in cycle start
    if (store.select(isInCycle, grant.id)) {
      store.dispatch(resetGrant, grant.id)
    }


    // Pay out grant
    if (store.select(remainingOfGrant, grant.id) > 0) {

      // Pick one receiver of all that are eligible
      const project  = randomizer.pick(
        store.select(eligibleForGrant, grant.id))

      // Did find receiver?
      if (project != null) {

        // Determine size of claim
        const cost = store.select(remainingStageCost, project.id)
        const frac = randomizer.sample(store.select(grantFraction, grant.id)).value
        const claim = cost * frac

        // Try claim
        const amount = Math.ceil(store.select(checkClaim, grant.id, claim))

        // Find stage in question
        const stage  = store.select(nextStage, project.id)

        store.dispatch(provideGrant, project.id, amount)
        store.dispatch(markAsHavingReceivedGrant, stage.id)
        store.dispatch(reduceRemainingGrant, grant.id, amount)

        // Leave hibernation if in hibernation?
        if (
          store.select(isHibernating,   project.id) && // hibernating?
          store.select(affordsNextStep, project.id)    // enough $$$?
        ) {
          store.dispatch(markAsInitializing, project.id)
        }

      }
    }
  }
}
