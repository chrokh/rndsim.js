import { eligibleForPartnership    } from '../selectors/projects'
import { owner                     } from '../selectors/organizations'
import { potentialPartners         } from '../selectors/organization-types'
import { build                     } from '../builders/organizations-from-types'
import { verticalValue as evaluate } from '../queries/valuations'
import { costOfCapital, threshold  } from '../properties/organization'
import { add                       } from '../actions/organizations'
import { associatePartner          } from '../actions/projects'

export default (store, randomizer) => {

  const projects = store.select(eligibleForPartnership)

  for (const project of randomizer.shuffle(projects)) {

    const org        = store.select(owner, project.id)
    const potentials = store.select(potentialPartners, org.id)
    const spec       = randomizer.pick(potentials)

    if (spec != null) {
      const partner = build(spec, randomizer)
      const value   = evaluate(store, project.id, costOfCapital(partner))

      if (value >= threshold(partner)) {
        store.dispatch(add, partner)
        store.dispatch(associatePartner, project.id, partner.id)
      }
    }

  }
}
