const grantTypes = require('../selectors/grant-types').all
const resetGrantTo = require('../actions/grants').resetTo
const addMany  = require('../actions/grants').addMany
const build = require('../builders/grants')

module.exports = (store, randomizer) => {

  // For every grant
  for (const spec of store.select(grantTypes)) {

    // Spawn
    store.dispatch(addMany, build(spec, randomizer))

  }


}
