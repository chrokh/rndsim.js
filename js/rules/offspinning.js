const build                  = require('../builders/organizations-from-types')

const firstOwner             = require('../selectors/project-types').firstOwner
const researchers            = require('../selectors/researchers').all
const projects               = require('../selectors/projects').findMany

const add                    = require('../actions/generic').addNested
const transferFromResearcher = require('../actions/projects').transferFromResearcher



module.exports = function (store, randomizer) {

  // For every researcher
  for (const researcher of store.select(researchers)) {

    // For every project owned by that researcher
    for (const project of store.select(projects, researcher.projects)) {

      // Build the organization
      const organization = build(
        store.select(firstOwner, project.type), // find spec for project's first owner
        randomizer)

      // Add the organization
      store.dispatch(add, 'organizations', organization)

      // Transfer the project from the researcher to the organization
      store.dispatch(transferFromResearcher, project.id, researcher.id, organization.id)

    }
  }

}
