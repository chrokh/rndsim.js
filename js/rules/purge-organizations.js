const organizations      = require('../selectors/organizations').all
const hasInfiniteCapital = require('../selectors/organizations').hasInfiniteCapital
const hasProjects        = require('../selectors/organizations').hasProjects
const remove             = require('../actions/generic').removeRecord
const isAtStartOfCycle   = require('../selectors/simulation').isAtStartOfCycle


module.exports = (store, randomizer) => {

  if (store.select(isAtStartOfCycle)) {

    for (const organization of store.select(organizations)) {

      if (
        !store.select(hasProjects,        organization.id) &&
        !store.select(hasInfiniteCapital, organization.id)
      ) {
        store.dispatch(remove, 'organizations', organization.id)
      }

    }

  }

}
