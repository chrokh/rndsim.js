const _                 = require('lodash')
const build             = require('../builders/organizations-from-types')
const add               = require('../actions/organizations').add
const organizationTypes = require('../selectors/organization-types').all
const initialSpawns     = require('../selectors/organization-types').initialSpawns

module.exports = (store, randomizer) => {

  for (const spec of store.select(organizationTypes)) {

    _(store.select(initialSpawns, spec.id)).times(() => {
      store.dispatch(add, build(spec, randomizer))
    })

  }

}
