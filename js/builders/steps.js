module.exports = (stage, randomizer) => {

  // Round up to overestimate time rather than underestimate
  const numSteps = Math.ceil(stage.time)
  let reperiodized = new Array(numSteps)
  let t = 0

  // Precompute
  const prob = Math.pow(stage.prob, (1 / stage.time))
  const cash = (isNaN(stage.cash) ? stage.cash : (stage.cash / stage.time))
  const cost = (isNaN(stage.cost) ? stage.cost : (stage.cost / stage.time))

  while (t < numSteps) {
    reperiodized[t] = {

      id:                 randomizer.nextId(),

      prob,
      cash,
      cost,

      group:              stage.group,
      point_of_interest:  stage.point_of_interest,
      requires_patent:    stage.requires_patent,

    }
    t++
  }

  return reperiodized
}
