const _ = require('lodash')

const DEFAULT_STATE =
  require('../constants/project-states').INITIALIZING

const expectedValue = require('../calculations/distribution').expectedValue
const interpolate = require('../calculations/interpolate')
const buildStage = require('./stage')
const buildSteps = require('./steps')
const reducedByPatentLife = require('../calculations/periods').reducedByPatentLife


const stageHash = (stage, step) => ({
  stage:              stage.id,
  id:                 step.id,
  group:              step.group,
  point_of_interest:  step.point_of_interest,
  requires_patent:    step.requires_patent,
  prob:               step.prob,
  cash:               step.cash,
  cost:               step.cost,
})


module.exports = (spec, randomizer) => {

  // Sanity check
  if (isNaN(spec.timeOfDiscovery)) {
    throw new Error('Not given time of discovery')
  }

  // Re-used counter
  let i = 0

   // Build stages
  const numStages = spec.lifecycle.remaining.length
  let stages = new Array(numStages)
  while (i < numStages) {
    stages[i] = buildStage(spec.lifecycle.remaining[i], randomizer)
    i++
  }

  // Extract steps
  let steps = []
  for (const stage of stages) {
    for (const step of stage.steps) {
      steps.push(stageHash(stage, step))
    }
  }

  // Expand steps requiring interpolation
  const interpolatedSteps   = interpolate(steps, stages)

  // Calculate total time passed for initial state projects
  const passedTotals = aggregateTotalsWithoutPatent(spec.lifecycle.completed)

  // Calculate initial patent time remaining
  const remainingPatent = 20 * 12 - passedTotals.time // TODO: Un-hardcode

  // Calculate totals expected for project
  const futureTotals = aggregateTotalsFromSteps(
    reducedByPatentLife(interpolatedSteps, remainingPatent))


  return {

    // Identity
    id:                  randomizer.nextId(),

    // Extraction
    type:                spec.id,
    group:               spec.group,
    source:              spec.source,
    time_of_discovery:   spec.timeOfDiscovery,
    max_hibernation:     spec.max_hibernation,

    // Computed
    stages,
    steps:               interpolatedSteps,
    initial_time_passed: passedTotals.time,
    steps_passed:        0,

    peak_cash:           interpolatedSteps[interpolatedSteps.length - 1].cash,

    total_cash:          futureTotals.cash,
    total_cost:          passedTotals.cost + futureTotals.cost,
    total_time:          passedTotals.time + futureTotals.time,
    total_prob:          passedTotals.prob * futureTotals.prob,

    // Defaults
    state:                  DEFAULT_STATE,
    capital_spent:          0,
    grants_spent:           0,
    capital_accumulated:    0,
    grants_accumulated:     0,
    partners:               [],
    investments:            [],
    valuation:              undefined,
    entered_hibernation_at: undefined,
  }
}



const aggregateTotalsWithoutPatent = (stages) => {
  let sum = 0
  let result = {
    cost: 0,
    time: 0,
    prob: 1,
  }

  for (const stage of stages) {
    result.cost += Math.ceil(expectedValue(stage.cost))
    result.time += Math.ceil(expectedValue(stage.time))
    result.prob *= expectedValue(stage.prob)
  }

  return result
}

// TODO: These two aggregate functions are essentially the same
// and should be merged.
const aggregateTotalsFromSteps = (steps) => {
  let result = {
    cost: 0,
    cash: 0,
    time: 0,
    prob: 1,
  }

  let has_seen_poi = false
  for (const step of steps) {

    // Add cash all the way up to end of patent
    result.cash += step.cash

    if (!has_seen_poi) {
      if (step.point_of_interest) {
        has_seen_poi = true
      } else {
        // Add cost/prob/time only up to Market Entry (POI)
        result.cost += step.cost
        result.time += 1
        result.prob *= step.prob
      }
    }
  }

  return result
}


