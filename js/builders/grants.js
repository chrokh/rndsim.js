module.exports = (spec, randomizer) => {

  const size = randomizer.sample(spec.size).value

  let fraction
  if (spec.fraction)
    fraction = randomizer.sample(spec.fraction).value

  let grants = []

  for (const pool of spec.pools) {

    if (pool.fraction)
      fraction = randomizer.sample(pool.fraction).value

    grants.push({
      id:               randomizer.nextId(),
      type:             pool.id,
      target_stages:    pool.target_stages,
      target_projects:  pool.target_projects,
      cycle:            spec.cycle,
      fraction,
      size,
    })

  }

  return grants
}
