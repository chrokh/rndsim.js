module.exports = () => ({

  id:                 'NULL_STEP',

  prob:               1,
  cash:               0,
  cost:               0,

  point_of_interest:  false,
  requires_patent:    false,

})
