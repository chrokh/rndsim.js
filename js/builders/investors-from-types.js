const build = (spec, randomizer) => {
  return {
    id:                 randomizer.nextId(),
    type:               spec.id,
    cost_of_capital:    randomizer.sample(spec.cost_of_capital),
    threshold:          randomizer.sample(spec.threshold),
  }
}


module.exports = {
  build
}
