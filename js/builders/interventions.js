const _ = require('lodash')
const buildStage = require('./stage')



module.exports = (spec, randomizer) => {

  const stages =
    spec.stage_types.map(stage => buildStage(stage, randomizer))

  const steps =
    _.flatten(stages.map(stage => stage.steps.map(step => Object.assign({}, step, { stage: stage.id }))))

  return {
    id:               randomizer.nextId(),
    type:             spec.id,
    steps,
    stages,
    strategy:         spec.strategy,
    target_stage:     spec.target_stage,
    target_projects:  spec.target_projects,
  }
}

