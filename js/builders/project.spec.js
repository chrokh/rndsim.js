const expect = require('chai').expect
const Randomizer = require('../endpoints/randomizer')
const buildProject = require('./project')


describe('Builder.ProjectsFromTypes.nested', () => {
  const f = buildProject

  context('.total_time', () => {
    before(() => {
      this.spec = {
        timeOfDiscovery: 0,
        lifecycle: {
          remaining: [
            { cash: 1, prob: 1, cost: 1, time: 1,     point_of_interest: false, requires_patent: true },
            { cash: 1, prob: 1, cost: 1, time: 10,    point_of_interest: false, requires_patent: true },
            { cash: 1, prob: 1, cost: 1, time: 100,   point_of_interest: false, requires_patent: true },
            { cash: 1, prob: 1, cost: 1, time: 1000,  point_of_interest: true , requires_patent: true },
            { cash: 1, prob: 1, cost: 1, time: 10000, point_of_interest: false, requires_patent: true },
          ],
          completed: [ ]
        }
      }
    })

    it('totals up the time before point of interest', () => {
      expect(f(this.spec, new Randomizer()).total_time).to.eq(111)
    })
  })

  context('.cash', () => {

    before(() => {
      let randomizer = new Randomizer()

      const spec = {
        timeOfDiscovery: 0,
        lifecycle: {
          remaining: [
            { prob: (0.5 ** 2), time: 2, cost: 10, cash: 0, requires_patent: true },
            { prob: (0.6 ** 3), time: 3, cost: 30, cash: 0, requires_patent: true },
            { prob: 1, time: 1, cost: 0, cash: 10, requires_patent: true },
            { prob: (0.8 ** 2), time: 2, cost: 2, cash: 10, requires_patent: true }
          ],
          completed: [ ]
        }
      }

      this.actual = f(spec, randomizer)
    })

    it('extracts costs', () => {
      expect(this.actual.steps.map(s => s.cost)).
        to.eql([5, 5, 10, 10, 10, 0, 1, 1])
    })

    it('extracts cash', () => {
      expect(this.actual.steps.map(s => s.cash)).
        to.eql([0, 0, 0, 0, 0, 10, 5, 5])
    })

    it('extracts prob', () => {
      expect(this.actual.steps.map(s => s.prob)).
        to.eql([0.5, 0.5, 0.6, 0.6, 0.6, 1, 0.8, 0.8])
    })

    it('starts with 0 steps passed', () => {
      expect(this.actual.steps_passed).to.eq(0)
    })

  })

  context('where a stage has interpolated cost and cash', () => {

    before(() => {
      let randomizer = new Randomizer()

      const spec = {
        timeOfDiscovery: 0,
        lifecycle: {
          remaining: [
            { prob: 1, time: 2, cash: 20,       cost: 0,        requires_patent: true },
            { prob: 1, time: 5, cash: 'linear', cost: 'linear', requires_patent: true },
            { prob: 1, time: 4, cash: 160,      cost: 24,       requires_patent: true },
          ],
          completed: [ ]
        }
      }

      this.actual = f(spec, randomizer)
    })

    it('interpolates cash', () => {
      expect(this.actual.steps.map(s => s.cash)).
        to.eql([
          10, 10,
          15, 20, 25, 30, 35,
          40, 40, 40, 40])
    })

    it('interpolates cost', () => {
      expect(this.actual.steps.map(s => s.cost)).
        to.eql([
          0, 0,
          1, 2, 3, 4, 5,
          6, 6, 6, 6])
    })

  })

})
