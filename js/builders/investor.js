module.exports = (spec, randomizer) => {

  // TODO: TEMPORARY HACK THAT STARTS IN rules/spawn-investors.js
  const thresh = randomizer.sample(spec.threshold).value * spec.multiplier

  return {

    // Identity
    id:               randomizer.nextId(),
    type:             spec.id,

    // Delegation
    targets:          spec.targets,
    compounder:       spec.compounder,

    // Sampled
    cost_of_capital:  randomizer.sample(spec.cost_of_capital).value,
    fraction:         randomizer.sample(spec.fraction).value,
    threshold:        thresh

  }
}
