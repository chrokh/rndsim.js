const buildSteps = require('./steps')
const expectedValue = require('../calculations/distribution').expectedValue


// TODO: Test this
module.exports = (spec, randomizer, sample) => {


  const props = {
    point_of_interest:  spec.point_of_interest,
    requires_patent:    spec.requires_patent,
    group:              spec.group,
    prob:               randomizer.sample(spec.prob).value,
    cost:               randomizer.sample(spec.cost).value,
    cash:               randomizer.sample(spec.cash).value,
    time:               randomizer.sample(spec.time).value,
  }

  return {

    // Identity
    id:             randomizer.nextId(),

    // Extraction
    group:          spec.group,
    type:           spec.id,

    // Computed
    prob:           props.prob,
    cost:           props.cost,
    cash:           props.cash,
    time:           props.time,

    // Nested
    steps:          buildSteps(props, randomizer),

    // Defaults
    steps_passed:   0,

  }
}
