module.exports = (spec, randomizer) => {
  return {
    id:                 randomizer.nextId(),
    type:               spec.id,
    cost_of_capital:    randomizer.sample(spec.cost_of_capital),
    threshold:          randomizer.sample(spec.threshold),
    potential_buyers:   spec.potential_buyers,
    infinite_capital:   spec.infinite_capital,
    projects:           [], // TODO: FK should probably be in other end
    compounder:         spec.compounder,
  }
}
