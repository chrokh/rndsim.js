const deepmerge = require('deepmerge')


const merge = (a, b) =>
  deepmerge(Object.assign({}, a), Object.assign({}, b), { arrayMerge })

const arrayMerge = (first, second, options) => {
  let result = first
  for (let item of second) {
    if (!result.find(x => x == item)) {
      if (item.id) {
        const old = result.find(x => x.id == item.id)
        result = result.filter(x => x.id != item.id)
        if (old) {
          item = merge(old, item, { arrayMerge })
        }
      }
      result.push(item)
    }
  }
  return result
}


module.exports = {
  merge
}
