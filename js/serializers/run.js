const projects      = require('../selectors/projects').all
const hibernating   = require('../selectors/projects').hibernating
const organizations = require('../selectors/organizations').all
const currentRun    = require('../selectors/simulation').currentRun
const currentTick   = require('../selectors/simulation').currentTick

module.exports = (state) => {

  return {
    RUN:           currentRun(state),
    ticks:         currentTick(state),
    projects:      projects(state).length,
    hibernating:   hibernating(state).length,
    organizations: organizations(state).length,
  }

}
