const currentTick   = require('../selectors/simulation').currentTick
const currentRun    = require('../selectors/simulation').currentRun
const projects      = require('../selectors/projects').all
const organizations = require('../selectors/organizations').all


module.exports = (state) => {

  return {
    RUN:                currentRun(state),
    TICK:               currentTick(state),
    num_projects:       projects(state).length,
    num_organizations:  organizations(state).length,
  }

}
