const Projects      = require('../selectors/projects')
const Grants        = require('../selectors/grants')
const Investors     = require('../selectors/investors')
const InvestorTypes = require('../selectors/investor-types')
const Stages        = require('../selectors/stages')
const StageTypes    = require('../selectors/stage-types')
const Organizations = require('../selectors/organizations')
const evaluate      = require('../queries/evaluate')
const Interventions = require('../selectors/interventions')
const currentTick   = require('../selectors/simulation').currentTick
const currentRun    = require('../selectors/simulation').currentRun
const mean          = require('../math/combinators').equally
const Distribution  = require('../calculations/distribution')


module.exports = (state, cache) => {

  const projects = []

  const meanThreshold = mean(Organizations.withInfiniteCapital(state).map(o => o.threshold.value))
  const meanCostOfCap = mean(Organizations.withInfiniteCapital(state).map(
    o => o.cost_of_capital.value / o.compounder))

  // Save info about investors in cache
  // TODO: Should this really happen here?
  let investors = cache.grab('investors', () => {
    let investors = {}
    for (const investor of Investors.all(state)) {
      for (const target of investor.targets) {
        investors[target] = investors[target] || {}
        investors[target].rates = investors[target].rates || []
        let rate = Investors.costOfCapital(state, investor.id)
        let compounder = Investors.compounder(state, investor.id)
        investors[target].rates.push(rate / compounder)
        investors[target].rate = mean(investors[target].rates)
      }
    }
    investors.mean = mean(Object.keys(investors).map(key => investors[key].rate))
    return investors
  })




  for (const proj of Projects.all(state)) {

    const organization  = Projects.owner(state, proj.id)
    const costOfCapital = Projects.compositeCostOfCapital(state, proj.id)

    let grantsPotential = 0
    for (const grant of Grants.all(state)) {
      if (grant.target_projects.includes(proj.group)) {
        for (const stageId of proj.stages) {
          const stage = Stages.find(state, stageId)
          if (grant.target_stages.includes(stage.group)) {
            const expectedCost = Distribution.expectedValue(StageTypes.find(state, stage.type))
            grantsPotential += stage.cost * grant.fraction
          }
        }
      }
    }

    // Construct output row
    projects.push({

      // Uniq
      RUN:   currentRun(state),
      TICK:  currentTick(state),
      PROJ:  proj.id,

      // Interventions
      intervention:            Interventions.activeType(state),
      interventions_tot_size:  Interventions.totalSize(state),

      // Grants
      grants_tot_size:                cache.grab('grants', () => Grants.totalSize(state)),
      grants_avg_frac:                cache.grab('grantfrac', () => parseFloat(Grants.averageFraction(state).toFixed(4))),

      // Investors
      inv_rate:                       parseFloat(investors.mean.toFixed(4)),

      // Organizations
      orgs_infcap_thresh:             meanThreshold,
      orgs_infcap_rate:               parseFloat(meanCostOfCap.toFixed(4)),

      // Project and grants
      proj_grants_potential:          parseFloat(grantsPotential.toFixed(4)),

      // Project and market
      proj_peak_cash:                 parseFloat(proj.peak_cash.toFixed(4)),
      proj_tot_cost:                  parseFloat(proj.total_cost.toFixed(4)),
      proj_tot_cash:                  parseFloat(proj.total_cash.toFixed(4)),
      proj_tot_prob:                  parseFloat(proj.total_prob.toFixed(4)),
      proj_tot_time:                  proj.total_time,

      // Project
      proj_group:                     Projects.group(state, proj.id),
      proj_type_discovery_rate:       parseFloat(Projects.discoveryRate(state, proj.id).toFixed(4)),
      proj_stage_group:               Projects.nextStepGroup(state, proj.id),
      proj_state:                     Projects.state(state, proj.id),
      proj_owner:                     organization.id,
      proj_owner_type:                organization.type,
      proj_source:                    Projects.source(state, proj.id),
      proj_composite_valuation:       parseFloat(
        evaluate(state, cache, proj.id, Projects.compositeCostOfCapital(state, proj.id)).toFixed(4)),
      proj_partners:                  Projects.partners(state, proj.id).length,
      proj_investors:                 Projects.investorIds(state, proj.id).length,
      proj_grants_accumulated:        parseFloat(Projects.grantsAccumulated(state, proj.id).toFixed(4)),
      proj_capital_accumulated:       parseFloat(Projects.capitalAccumulated(state, proj.id).toFixed(4)),
      proj_capital_spent:             parseFloat(Projects.capitalSpent(state, proj.id).toFixed(4)),
      proj_grants_spent:              parseFloat(Projects.grantsSpent(state, proj.id).toFixed(4))
    })
  }

  return projects
}

