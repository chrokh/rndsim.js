const addNested = require('./generic').addNested

const add = (state, entity) =>
  addNested(state, 'interventions', entity)


module.exports = {
  add
}
