const normalize   = require('../synthesizers/normalizer').normalize
const denormalize = require('../synthesizers/normalizer').denormalize
const isolate     = require('../selectors/generic').isolate


const add = (state, entity, obj) => {
  const item = {}
  item[obj.id] = obj

  const change = {}
  change[entity] = Object.assign({}, state[entity], item)

  const updated = Object.assign({}, state, change)

  return updated
}

const removeRecord = (state, entity, id) => {
  const records = Object.assign({}, state[entity])
  delete records[id]

  const change = {}
  change[entity] = records

  return Object.assign({}, state, change)
}


const removeRecords = (state, entity, ids) => {
  const records = Object.assign({}, state[entity])

  for (const id of ids) {
    delete records[id]
  }

  const change = {}
  change[entity] = records

  return Object.assign({}, state, change)
}


const removeCascade = (state, entity, id) => {
  const isolated = isolate(state, entity, id)
  return Object.keys(isolated).reduce((_state, key) => {
    if (
      Object.keys(isolated[key]).length > 0 &&
      key != 'simulations'
    ) {
      return removeRecords(_state, key, Object.keys(isolated[key]))
    } else {
      return _state
    }
  }, state)
}


const addSet = (state, entity, objs) => {
  const item = {}
  for (const key in objs) {
    item[objs[key].id] = objs[key]
  }

  const change = {}
  change[entity] = Object.assign({}, state[entity], item)

  const updated = Object.assign({}, state, change)

  return updated
}


const updateRecord = (state, entity, id, f) => {
  const original = state[entity][id]
  const change   = f(original)
  const updated  = Object.assign({}, original, change)
  return replaceRecord(state, entity, id, updated)
}

const addNested = (state, entity, obj) => {
  const denormalized = {}
  denormalized[entity] = [obj]

  const flat = normalize(denormalized)

  for (const key in flat) {
    if (key != 'simulations') {
      state = addSet(state, key, flat[key])
    }
  }

  return state
}

const associateRecord = (state, entity, id, property, fk) => {
  return updateRecord(state, entity, id, (record) => {
    const update = {}
    update[property] = (record[property] || []).concat([fk])
    return update
  })
}

const unassociateRecord = (state, entity, id, property, fk) => {
  return updateRecord(state, entity, id, (record) => {
    const update = {}
    update[property] = record[property].filter(item => item != fk)
    return update
  })
}

const replaceRecord = (state, entity, id, replacement) => {
  const recordChange = {}
  recordChange[id] = replacement

  const change = {}
  change[entity] = Object.assign({}, state[entity], recordChange)

  return Object.assign({}, state, change)
}


module.exports = {
  add,
  addSet,
  addNested,
  removeRecord,
  removeRecords,
  removeCascade,
  updateRecord,
  associateRecord,
  unassociateRecord
}
