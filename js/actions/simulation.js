const incrementTick = (state) => {

  const oldSim = state.simulations[undefined]

  const newSim = Object.assign({}, oldSim, {
    tick: oldSim.tick + 1
  })

  const newState = Object.assign({}, state, {
    simulations: { undefined: newSim }
  })

  return newState
}


const incrementRun = (state) => {

  const oldSim = state.simulations[undefined]

  const newSim = Object.assign({}, oldSim, {
    run: oldSim.run + 1
  })

  const newState = Object.assign({}, state, {
    simulations: { undefined: newSim }
  })

  return newState
}


module.exports = {
  incrementTick,
  incrementRun,
}
