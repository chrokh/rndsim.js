const expect = require('chai').expect
const Generic = require('./generic-mutable')


const state = {
  projects: {
    1:   { id: 1,   episodes: [2, 3]     },
    100: { id: 100, episodes: [101, 102] },
  },

  stages: {
    1:   { id: 1 },
    100: { id: 100 },
  },
}


describe('updateRecord', () => {

  it('finds the record and updates it', () => {
    const actual = Generic.updateRecord(state, 'projects', 100, (record) => ({
      episodes: 'changed'
    }))
    expect(state).to.eql({
      projects: {
        1:   { id: 1,   episodes: [2, 3]     },
        100: { id: 100, episodes: 'changed'  },
      },
      stages: {
        1:   { id: 1 },
        100: { id: 100 },
      },
    })
  })


})

