const expect = require('chai').expect
const removeCascade = require('./generic').removeCascade


const state = {
  projects: {
    1:   { id: 1,   episodes: [2, 3]     },
    100: { id: 100, episodes: [101, 102] },
  },

  episodes: {
    2: { id: 2 },
    3: { id: 3 },
    101: { id: 101, stages: [1000] },
    102: { id: 102 },
  },

  stages: {
    1000:  { id: 1000 },
    10000: { id: 10000 },
  },
}


xdescribe('removeCascade', () => {

  it('removes dependencies without removing other records', () => {
    const actual = removeCascade(state, 'projects', 100)
    expect(actual.projects[1]).to.not.be.undefined
    expect(actual.episodes[2]).to.not.be.undefined
    expect(actual.episodes[3]).to.not.be.undefined
    expect(actual.stages[10000]).to.not.be.undefined
    expect(actual.projects[100]).to.be.undefined
    expect(actual.episodes[101]).to.be.undefined
    expect(actual.episodes[102]).to.be.undefined
    expect(actual.stages[1000]).to.be.undefined
  })

})
