const _add = require('./generic').add


const add = (state, investor) =>
  _add(state, 'investors', investor)


module.exports = {
  add
}
