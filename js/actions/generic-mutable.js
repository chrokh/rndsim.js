const normalize   = require('../synthesizers/normalizer').normalize
const denormalize = require('../synthesizers/normalizer').denormalize
const isolate     = require('../selectors/generic').isolate


const addIndex = (state, entity, id) => {
  state.index[entity] = state.index[entity] || []
  state.index[entity].push(id)
  return state
}


const add = (state, entity, obj) => {
  state[entity][obj.id] = obj
  return state
}

const addMany = (state, entity, objs) => {
  for (const obj of objs) {
    state[entity][obj.id] = obj
  }
  return state
}

const removeRecord = (state, entity, id) => {
  delete state[entity][id]
  return state
}


const removeRecords = (state, entity, ids) => {
  for (const id of ids) {
    delete state[entity][id]
  }
  return state
}


const removeCascade = (state, entity, id) => {
  // TODO: We have a bug here. If I don't call
  // the non-cascading version then we loose an
  // organization we didn't intend to loose. It's
  // probably somehow because of the double FK
  // situation. We actually want the cascading implementation!
  return removeRecord(state, entity, id)
  //const isolated = isolate(state, entity, id)
  //for (const key in isolated) {
  //  if (key != 'simulations') {
  //    removeRecords(state, key, Object.keys(isolated[key]))
  //  }
  //}
  //return state
}


const addSet = (state, entity, objs) => {
  for (const key in objs) {
    state[entity][key] = objs[key]
  }
  return state
}


const updateRecord = (state, entity, id, f) => {
  const change = f(state[entity][id])
  for (var key in change) {
    state[entity][id][key] = change[key]
  }
  return state
}

const addNested = (state, entity, obj) => {
  const denormalized = {}
  denormalized[entity] = [obj]

  const flat = normalize(denormalized)

  for (const key in flat) {
    if (key != 'simulations') {
      addSet(state, key, flat[key])
    }
  }

  return state
}

const associateRecord = (state, entity, id, property, fk) => {
  if (state[entity][id][property] == null) {
    state[entity][id][property] = []
  }
  state[entity][id][property].push(fk)
  return state
}

const unassociateRecord = (state, entity, id, property, fk) => {
  const arr = state[entity][id][property]
  arr.splice(arr.indexOf(fk), 1)
  return state
}

const replaceRecord = (state, entity, id, replacement) => {
  state[entity][id] = replacement
  return state
}


module.exports = {
  add,
  addSet,
  addMany,
  addIndex,
  addNested,
  removeRecord,
  removeCascade,
  updateRecord,
  associateRecord,
  unassociateRecord
}
