const _addMany = require('./generic').addMany
const updateRecord = require('../actions/generic').updateRecord
const grantSize    = require('../selectors/grants').size


const addMany = (state, grants) =>
  _addMany(state, 'grants', grants)


const reset = (state, grantId) =>
  updateRecord(state, 'grants', grantId, (grant) => ({
    remaining: grantSize(state, grant.id)
  }))


const reduceRemaining = (state, grantId, amount) =>
  updateRecord(state, 'grants', grantId, (grant) => ({
    remaining: grant.remaining - amount
  }))


module.exports = {
  reset,
  addMany,
  reduceRemaining,
}
