const _add = require('./generic').add
const _addIndex = require('./generic').addIndex


const add = (state, organization) => {
  state = _add(state, 'organizations', organization)

  // TODO: This makes lookup of 'infinite-capital-organizations' faster,
  // but it is not a very elegant solution. Perhaps Giants and Workers
  // should be modeled as two 'subtypes' (ish) of Organizations, rather
  // than as the property: infinite_capital?
  // PS. This is caching, so as soon as infinite_capital becomes a
  // mutable property, this is a dangerous solution!
  if (organization.infinite_capital) {
    state = _addIndex(state, 'giants', organization.id)
  }
  return state
}


module.exports = {
  add
}
