const associateRecord      = require('./generic').associateRecord
const unassociateRecord    = require('./generic').unassociateRecord
const updateRecord         = require('./generic').updateRecord
const updateRecordProperty = require('./generic').updateRecordProperty
const removeCascade        = require('./generic').removeCascade
const nextStepCost         = require('../selectors/projects').nextStepCost
const owner                = require('../selectors/projects').owner
const _add                 = require('../actions/generic').addNested
const Project              = require('../entities/project')


const add = (state, project) =>
  _add(state, 'projects', project)

const remove = (state, projectId) => {
  const ownerId = owner(state, projectId).id
  state = unassociateRecord(state, 'organizations', ownerId, 'projects', projectId)
  state = removeCascade(state, 'projects', projectId)
  return state
}

const addProjectToResearcher = (state, projectId, researcherId) => {
  return associateRecord(state, 'researchers', researcherId, 'projects', projectId)
}

const transferFromResearcher = (state, projectId, researcherId, organizationId) => {
  state = unassociateRecord(state, 'researchers', researcherId, 'projects', projectId)
  // NOTE: Denormalized (i.e. FK in both ends) for performance reasons
  state = associateRecord(state, 'organizations', organizationId, 'projects', projectId)
  state = updateRecordProperty(state, 'projects', projectId, 'owner', organizationId)
  return state
}

const addToOrganization = (state, projectId, orgId) => {
  state = updateRecordProperty(state, 'projects', projectId, 'owner', orgId)
  state = associateRecord(state, 'organizations', orgId,  'projects', projectId)
  return state
}

const transfer = (state, projectId, sellerId, buyerId) => {
  state = updateRecordProperty(state, 'projects', projectId, 'owner', buyerId)
  // NOTE: Denormalized (i.e. FK in both ends) for performance reasons
  state = unassociateRecord(state, 'organizations', sellerId, 'projects', projectId)
  state = associateRecord(state,   'organizations', buyerId,  'projects', projectId)
  return state
}

const associatePartner = (state, projectId, partnerId) =>
  associateRecord(state, 'projects', projectId, 'partners', partnerId)

const associateInvestor = (state, projectId, investorId, stageGroup) =>
  associateRecord(state, 'projects', projectId, 'investments', {
    investor: investorId,
    stageGroup
  })

const payForNextStep = (state, projectId) => {
  return updateRecord(state, 'projects', projectId, (project) => {
    // Always use grant money first, then investment capital
    const cost  = nextStepCost(state, projectId)
    const proj  = new Project(state, projectId)
    const split = proj.splitCost(cost)
    return {
      grants_spent:  project.grants_spent + split.grants,
      capital_spent: project.capital_spent + split.capital,
    }
  })
}

const provideCapital = (state, projectId, capital) =>
  updateRecord(state, 'projects', projectId, (project) => ({
    capital_accumulated: (project.capital_accumulated + capital)
  }))

const provideGrant = (state, projectId, capital) =>
  updateRecord(state, 'projects', projectId, (project) => ({
    grants_accumulated: (project.grants_accumulated + capital)
  }))

const incrementStep = (state, id) =>
  updateRecord(state, 'projects', id, (project) => ({
    steps_passed: (project.steps_passed + 1)
  }))





module.exports = {
  add,
  remove,
  addToOrganization,
  addProjectToResearcher,
  transferFromResearcher,
  transfer,
  associatePartner,
  associateInvestor,
  payForNextStep,
  provideCapital,
  provideGrant,
  incrementStep,
}
