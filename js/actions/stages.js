const updateRecord = require('./generic').updateRecord


const markAsHavingReceivedGrant = (state, id) =>
  updateRecord(state, 'stages', id, (stage) => ({
    has_received_grant: true
  }))




module.exports = {
  markAsHavingReceivedGrant,
}
