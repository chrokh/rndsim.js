const States        = require('../constants/project-states')
const updateRecord  = require('./generic').updateRecord
const currentTick   = require('../selectors/simulation').currentTick


const markAsTerminated = (state, projectId) =>
  transitionTo(state, projectId, States.TERMINATED)

const markAsDeveloping = (state, projectId) =>
  transitionTo(state, projectId, States.DEVELOPING)

const markAsFailed = (state, projectId) =>
  transitionTo(state, projectId, States.FAILED)

const markAsHibernating = (state, projectId) => {
  state = updateRecord(state, 'projects', projectId, () => ({
    entered_hibernation_at: currentTick(state)
  }))
  return transitionTo(state, projectId, States.HIBERNATING)
}

const markAsInitializing = (state, projectId) =>
  transitionTo(state, projectId, States.INITIALIZING)

const markAsExhausted = (state, projectId) =>
  transitionTo(state, projectId, States.EXHAUSTED)

const markAsCompleted = (state, projectId) =>
  transitionTo(state, projectId, States.COMPLETED)

const markAsDeciding = (state, projectId) =>
  transitionTo(state, projectId, States.DECIDING)


const transitionTo = (storeState, projectId, state) =>
  updateRecord(storeState, 'projects', projectId, () => ({ state }))



module.exports = {
  markAsInitializing,
  markAsHibernating,
  markAsTerminated,
  markAsDeveloping,
  markAsDeciding,
  markAsExhausted,
  markAsCompleted,
  markAsFailed,
}
