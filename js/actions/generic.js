
/*
 * TODO: This indirection exists so that we can switch
 * between mutable and immutable to make sure that
 * the mutable store actions indeed produce the same
 * results as the immutable.
 */

const Generic = require('./generic-mutable')



Generic.updateRecordProperty = (state, entity, id, property, value) =>
  Generic.updateRecord(state, entity, id, (record) => {
    const change = {}
    change[property] = value
    return change
  })



module.exports = Generic
