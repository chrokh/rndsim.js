const _ = require('lodash')
const reducedByPatentLife = require('../calculations/periods').reducedByPatentLife
const maxOf = require('../math/general').maxOf


module.exports = (project, intervention, patentLifeLeft) => {
  if (isTargetProject(project, intervention)) {
    return interventions[intervention.strategy](intervention, patentLifeLeft)
  } else {
    return interventions.NONE(intervention, patentLifeLeft)
  }
}


const REPLACE = (intervention, patentLifeLeft) =>
  (steps) =>
    rebuild(steps, intervention, patentLifeLeft, (original, replacement) => {
      if (replacement != undefined) {
        return simplify(replacement)
      } else {
        return simplify(original)
      }
    })


const ADD = (intervention, patentLifeLeft) =>
  (steps) =>
    rebuild(steps, intervention, patentLifeLeft, (original, addition) => {
      if (addition != undefined) {
        return {
          cash: original.cash + addition.cash,
          cost: original.cost + addition.cost,
          prob: original.prob, // TODO: This is INCORRECT overestimation.
        }
      } else {
        return simplify(original)
      }
    })


const NONE = (intervention, patentLifeLeft) =>
  (steps) => reducedByPatentLife(steps, patentLifeLeft)


const interventions = { NONE, REPLACE, ADD }


const rebuild = (steps, intervention, patentLifeLeft, merge) => {
  let hasSeen = false
  const zip = (original, alternative) => {
    if (original != undefined && intervention.target_stage == original.group) {
      hasSeen = true
    }

    if (!hasSeen && alternative == undefined) {
      return simplify(original)
    } else if(hasSeen && alternative == undefined && original != undefined) {
      return undefined
    } else if(hasSeen && alternative != undefined && original == undefined) {
      return simplify(alternative)
    } else {
      return merge(original, alternative)
    }
  }

  // Find index of target stage
  const index = steps.findIndex(s => s.group == intervention.target_stage)

  if (index != -1) {
    const whitespace = _.times(index, _.constant(undefined))
    const alternatives = whitespace.concat(intervention.steps)

    const patentReducedSteps = reducedByPatentLife(steps, patentLifeLeft)
    const patentReducedAlternatives = reducedByPatentLife(alternatives, patentLifeLeft)

    const items = maxOf([patentReducedSteps.length, patentReducedAlternatives.length])

    const newSteps = []
    for (let i=0; i<items; i++) {
      newSteps.push(zip(patentReducedSteps[i], patentReducedAlternatives[i]))
    }
    return _.compact(newSteps)
  } else {
    // Return original because steps do not contain target stage
    return steps
  }
}


const simplify = (step) => ({
  cost: step.cost,
  prob: step.prob,
  cash: step.cash,
})


const isTargetProject = (project, intervention) =>
  intervention.target_projects.indexOf(project.group) != -1

