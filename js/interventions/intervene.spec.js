const expect = require('chai').expect
const intervene = require('./intervene')



describe('intervene', () => {
  this.patentTimeLeft = 999

  beforeEach(() => {
    this.project = {
      group: 'P',
      steps: [
        { cash: 1,    cost: 2,    prob: 0.2, group: '-', requires_patent: false },
        { cash: 10,   cost: 20,   prob: 0.3, group: '-', requires_patent: false },
        { cash: 100,  cost: 200,  prob: 0.4, group: 'S', requires_patent: false },
        { cash: 1000, cost: 2000, prob: 0.6, group: 'S', requires_patent: false },
      ]
    }
  })


  context('replacement', () => {
    beforeEach(() => {
      this.intervention = {
        strategy:        'REPLACE',
        target_stage:    'S',
        target_projects: ['P'],
        steps: [
          { cash: 111, cost: 11, prob: 0.1, requires_patent: false },
          { cash: 222, cost: 22, prob: 0.2, requires_patent: false },
        ],
      }
    })

    it('replaces all steps after the target', () => {
      const actual = intervene(this.project, this.intervention, this.patentTimeLeft)(this.project.steps)
      expect(actual).to.eql([
        { cash: 1,   cost: 2,  prob: 0.2 },
        { cash: 10,  cost: 20, prob: 0.3 },
        { cash: 111, cost: 11, prob: 0.1 },
        { cash: 222, cost: 22, prob: 0.2 },
      ])
    })

  })


  context('addition', () => {
    beforeEach(() => {
      this.intervention = {
        strategy:        'ADD',
        target_stage:    'S',
        target_projects: ['P'],
        steps: [
          { cash: 111, cost: 11, prob: 0.1, requires_patent: false },
          { cash: 222, cost: 22, prob: 0.2, requires_patent: false },
        ],
      }
    })

    it('merges all steps after the target', () => {
      const actual = intervene(this.project, this.intervention, this.patentTimeLeft)(this.project.steps)
      expect(actual).to.eql([
        { cash: 1,    cost: 2,    prob: 0.2 },
        { cash: 10,   cost: 20,   prob: 0.3 },
        { cash: 211,  cost: 211,  prob: 0.4 },
        { cash: 1222, cost: 2022, prob: 0.6 },
      ])
    })

  })



  context('none', () => {
    beforeEach(() => {
      this.intervention = {
        strategy:        'NONE',
        target_stage:    'S',
        target_projects: ['P'],
        steps: [
          { cash: 999, cost: 99, prob: 1, requires_patent: false },
          { cash: 999, cost: 99, prob: 1, requires_patent: false },
        ],
      }
    })

    it('merges all steps after the target', () => {
      const actual = intervene(this.project, this.intervention, this.patentTimeLeft)(this.project.steps)
      expect(actual).to.eql(this.project.steps)
    })

  })
})
