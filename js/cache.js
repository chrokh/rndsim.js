// TODO: PLEASE UNIT TEST ME
module.exports = class Cache {

  constructor (limit = 10000) {
    this._limit = limit
    this._cache = {}
    this._count = 0
  }

  grab (key, f) {
    this._count ++
    this.clean()
    if (this._cache.hasOwnProperty(key)) {
      return this._cache[key]
    } else {
      return this._cache[key] = f()
    }
  }

  clean () {
    if (this._count > this._limit) {
      this._count = 0
      this._cache = {}
    }
  }

}
