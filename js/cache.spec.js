const expect = require('chai').expect
const Cache  = require('./cache')


describe('Cache', () => {

  it('caches based on key', () => {
    var counter = 0
    const cache = new Cache()
    const f = () => { counter++; return 'some-value'; }
    expect(cache.grab('some-key', f)).to.eq('some-value')
    expect(counter).to.eq(1)
    expect(cache.grab('some-key', f)).to.eq('some-value')
    expect(counter).to.eq(1)
    expect(cache.grab('some-other-key', () => {})).to.eq(undefined)
    expect(counter).to.eq(1)
  })

  it('destroys cache when it reaches limit', () => {
    const cache = new Cache(3)
    expect(cache.grab('some-key', () => 'value1')).to.eq('value1')
    expect(cache.grab('some-key', () => {})).to.eq('value1')
    expect(cache.grab('some-key', () => {})).to.eq('value1')
    expect(cache.grab('some-key', () => 'value2')).to.eq('value2')
    expect(cache.grab('some-key', () => {})).to.eq('value2')
    expect(cache.grab('some-key', () => {})).to.eq('value2')
  })


})
