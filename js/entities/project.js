const Stages   = require('../selectors/stages')
const Projects = require('../selectors/projects')


module.exports = class Project {

  constructor (state, id) {
    this.state   = state
    this.id      = id
    this.__cache = {}
  }

  stagesRemaining() {
    return this._cache('remaining', () =>
      Stages.remaining(this.state, this.id))
  }

  timeSpent() {
    return this._cache('timespent', () =>
      Projects.timeSpent(this.state, this.id))
  }

  grantsAvailable() {
    return this._cache('grants', () =>
      Projects.grantsAvailable(this.state, this.id))
  }

  splitCost(cost) {
    return this._cache('split', () => {
      const grants  = this.grantsAvailable() >= cost ? cost : this.grantsAvailable()
      const capital = cost - grants
      return { grants, capital }
    })
  }

  _cache(key, f) {
    if (this.__cache[key] == undefined) {
      this.__cache[key] = f()
    }
    return this.__cache[key]
  }

}
