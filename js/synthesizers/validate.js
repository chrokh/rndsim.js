const schema = require('../schema.json')
const Ajv    = require('ajv')


module.exports = function(data) {

  const ajv = new Ajv({
    unknownFormats: [
      'table',
      'grid',
      'tabs',
      'checkbox'
    ]
  })

  const valid = ajv.validate(schema, data)

  if (!valid) {
    throw new Error(`Input does not conform to schema. Error: ${JSON.stringify(ajv.errors)}`)
  }

  return data

}
