const _normalize   = require('normalizr').normalize
const _denormalize = require('normalizr').denormalize
const Schema       = require('normalizr').schema

const Entity = Schema.Entity


/// Initial state
const grant = new Entity('grants')
const stage = new Entity('stages')
const organization = new Entity('organizations')
const project = new Entity('projects', {
  stages: [ stage ],
  owner:  organization,
})
const researcher = new Entity('researchers', {
  projects: [ project ]
})
const intervention = new Entity('interventions', {
  stages: [ stage ]
})


/// Blueprint schemas
const grantType = new Entity('grant_types')
const investor = new Entity('investors')
const investorType = new Entity('investor_types')
const stageType = new Entity('stage_types')
const organizationType = new Entity('organization_types')
const interventionType = new Entity('intervention_types', {
  stage_types: [ stageType ]
})
const discoveryRate = new Entity('discovery_rates')
const lifecycle     = new Entity('lifecycles', {
  completed:       [ stageType ],
  remaining:       [ stageType ]
})
const projectType = new Entity('project_types', {
  first_owner:     organizationType,
  discovery_rate:  discoveryRate,
  lifecycle:       lifecycle,
  passed:          lifecycle,
})



/// Base schema
const schema = new Entity('simulations', {
  investor_types:      [investorType],
  investors:           [investor],
  organization_types:  [organizationType],
  stage_types:         [stageType],
  project_types:       [projectType],
  projects:            [project],
  researchers:         [researcher],
  organizations:       [organization],
  grants:              [grant],
  grant_types:         [grantType],
  intervention_types:  [interventionType],
  interventions:       [intervention],
  lifecycles:          [lifecycle],
  discovery_rates:     [discoveryRate],
})


/// Flat normalization
const normalize = (data) => {
  return Object.assign({}, {
    investor_types:      {},
    investors:           {},
    organization_types:  {},
    stage_types:         {},
    project_types:       {},
    projects:            {},
    stages:              {},
    researchers:         {},
    organizations:       {},
    grants:              {},
    grant_types:         {},
    intervention_types:  {},
    interventions:       {},
    lifecycles:          {},
    discovery_rates:     {},
  }, _normalize(data, schema).entities)
}


/// Nested denormalization
const denormalize = (data, spec) => {
  return _denormalize(spec, schema, data)
}


module.exports = {
  normalize,
  denormalize,
}
