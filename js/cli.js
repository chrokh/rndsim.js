#! /usr/bin/env node

const colors     = require('colors')
const glob       = require('glob')

const spawn      = require('child_process').spawn
const fs         = require('fs')
const path       = require('path')

const Simulation = require('./simulation')
const CSVWriter  = require('./endpoints/csv-writer')
const Sender     = require('./endpoints/sender')
const merge       = require('./utils').merge

const isAtStartOfCycle = require('./selectors/simulation').isAtStartOfCycle
const currentTick      = require('./selectors/simulation').currentTick
const currentRun       = require('./selectors/simulation').currentRun
const lastTick         = require('./selectors/simulation').lastTick
const lastRun          = require('./selectors/simulation').lastRun
const firstTick        = require('./selectors/simulation').firstTick
const firstRun         = require('./selectors/simulation').firstRun
const numberOfTicks    = require('./selectors/simulation').numberOfTicks
const numberOfRuns     = require('./selectors/simulation').numberOfRuns
const currentSeed      = require('./selectors/simulation').seed


/*
 * Helpers
 */

const printInfo = (message) =>
  console.log('>', ' INFO:'.yellow, message)

const printError = (message) =>
  console.log('>', 'ERROR:'.red, message)

const printInstructions = () => {
  console.log()
  console.log('USAGE')
  console.log('  rndsim <command>'.yellow)
  console.log()
  console.log('  Where <command> is one of:')
  console.log(`    ${'run'.blue}      Runs simulation`)
  console.log(`    ${'digest'.blue}   Restructures a simulation output file to simplify stats`)
  console.log()
  console.log('COMMANDS')
  console.log(`  ${'run'.yellow} ${'<input> <output>'.blue}`)
  console.log(`    ${'<input>'.red}   Input file in json format.`)
  console.log(`    ${'<output>'.red}  Output folder.`)
  console.log(`    ${'Example:  rndsim run input.json output'.gray}`)
  console.log()
  console.log(`  ${'digest'.yellow} ${'<csv> <output>'.blue}`)
  console.log(`    ${'<csv>'.red}     Output csv file produced by simulation.`)
  console.log(`    ${'<output>'.red}  Output folder.`)
  console.log(`    ${'Example:  rndsim digest all.csv output'.gray}`)
  console.log()
}




const doRun = (opts) => {

  // Extract input parameters
  const inputGlobs   = process.argv.slice(3, process.argv.length - 1)
  const outputFolder = process.argv[process.argv.length - 1]


  // Validate input parameters
  if (inputGlobs.length < 1) {
    printError('No input file provided!')
    printInstructions()
    return
  }

  // Validate output parameter
  if (outputFolder == null) {
    printError('No output folder provided!')
    printInstructions()
    return
  }

  // Identify input files (TODO: move elsewhere)
  const inputFiles = inputGlobs.
    map(input => {
      if (input.split('.').pop() == 'json')
        return input // file
      else
        return path.join(input, '*.json') // folder
    }).
    map(input => glob.sync(input, { nonull: true })).
    reduce((acc, item) => acc.concat(...item), [])

  // Merge input files (TODO: move elsewhere)
  const input = inputFiles.
    map(input => JSON.parse(fs.readFileSync(input, 'utf-8'))).
    reduce((acc, json) => merge(acc, json))

  // Write used input file to output directory
  const inputFile = path.join(outputFolder, '/input.json')
  fs.writeFileSync(inputFile, JSON.stringify(input, null, 2))

  // Construct meta file contents
  const metaFile = path.join(outputFolder, '/meta.json')
  const meta = {
    instances: [{
      id:  1,
      run: (input.run || 1)
    }]
  }

  // Write meta file
  fs.writeFileSync(metaFile, JSON.stringify(meta, null, 2))

  // Start simulation
  run({
    meta,
    input,
    inputFiles,
    outputFolder,
    writeHeaders: true
  })
}



const doAttach = (opts) => {

  // Extract input parameters
  const outputFolder = process.argv[3]

  // Validate input parameters
  if (outputFolder == null) {
    printError('No target folder provided!')
    return
  }

  // Read shared input file
  const inputFile = path.join(outputFolder, '/input.json')
  const input = JSON.parse(fs.readFileSync(inputFile))

  // Read meta file
  const metaFile = path.join(outputFolder, '/meta.json')
  const oldMeta  = JSON.parse(fs.readFileSync(metaFile))

  // Change meta info
  const lastInstance = oldMeta.instances[oldMeta.instances.length - 1]
  const instance = {
    id:  lastInstance.id + 1,
    run: lastInstance.run + input.number_of_runs,
  }
  const instances = [...oldMeta.instances, instance]
  const meta = Object.assign({}, oldMeta, { instances })

  // Write meta file
  fs.writeFileSync(metaFile, JSON.stringify(meta, null, 2))

  // Start simulation
  run({
    meta,
    input,
    outputFolder,
    inputFiles: [inputFile],
    writeHeaders: false
  })
}



const run = (config) => {

  // Find meta instance info
  const instance = config.meta.instances[config.meta.instances.length - 1]

  // Move info from meta to simulation input
  config.input.run  = instance.run
  config.input.seed = config.input.seed + instance.id

  // Create simulation
  var simulation = new Simulation(config.input, { sleep: 0 })

  simulation.on('beforeTick', function(store) {
    if (store.select(isAtStartOfCycle)) {
      printStatus({
        seed:          store.select(currentSeed),
        run:           store.select(currentRun),
        tick:          store.select(currentTick),
        firstRun:      store.select(firstRun),
        firstTick:     store.select(firstTick),
        lastTick:      store.select(lastTick),
        lastRun:       store.select(lastRun),
        numberOfRuns:  store.select(numberOfRuns),
        numberOfTicks: store.select(numberOfTicks),
      })
    }
  })

  const printStatus = (data) => {
    const relativeRun  = data.run  - data.firstRun + 1
    const relativeTick = data.tick - data.firstTick + 1
    process.stdout.write('\033c')
    printInfo(`Input:  [${config.inputFiles.join(', ')}]`)
    printInfo(`Output: ${config.outputFolder}`)
    console.log()
    console.log(`INSTANCE ID:   ${instance.id}`)
    console.log(`INSTANCE SEED: ${data.seed}`)
    console.log()
    console.log('PERCENTAGE')
    console.log(`RUN:   ${Math.round(relativeRun / data.numberOfRuns * 100)}%`)
    console.log(`TICK:  ${Math.round(relativeTick / data.numberOfTicks * 100)}%`)
    console.log()
    console.log('RELATIVE')
    console.log(`RUN:  ${data.run-data.firstRun+1}  (${data.numberOfRuns})`)
    console.log(`TICK: ${data.tick-data.firstTick+1} (${data.numberOfTicks})`)
    console.log()
    console.log('ABSOLUTE')
    console.log(`RUN:   ${data.run}  (${data.firstRun} => ${data.lastRun})`)
    console.log(`TICK:  ${data.tick} (${data.firstTick} => ${data.lastTick})`)
  }


  // Write output

  const writer = new CSVWriter(config.outputFolder, {
    writeHeaders: config.writeHeaders
  })

  const tickSender = new Sender(writer, {
    RUNS_TICKS_PROJECTS: require('./serializers/projects'),
  })

  simulation.on('beforeTick', (store) => {
    if (store.select(isAtStartOfCycle)) {
      tickSender.send(store.state, store.cache)
    }
  })


  // Run!
  simulation.loop()
}



const doSummarizeOutput = () => {
  const input  = process.argv[3]
  const output = process.argv[4]

  // TODO: This should be passed in and used as arg in rscript
  //const Combinatorics = require('js-combinatorics')
  //const groups = Combinatorics.power(['run', 'tick', 'proj_group', 'interventions_tot_size']).toArray()
  //groups.shift() // Remove first element since it is []

  console.log('Starting R')
  const rspawn = spawn('Rscript', ['./scripts/digest.R', input, output])

  rspawn.stdout.on('data', function (data) {
    process.stdout.write(data.toString())
  })

  rspawn.stderr.on('data', function (data) {
    console.log('STDERR:')
    console.log(data.toString())
  })

  rspawn.on('close', function (code) {
    console.log('R exited with code ' + code);
  });
}




/*
 * CLI
 */

const action = process.argv[2]

switch (action) {
  case 'run':
    doRun()
    break
  case 'attach':
    doAttach()
    break
  case 'digest':
    doSummarizeOutput()
    break
  default:
    printInstructions()
    break
}

