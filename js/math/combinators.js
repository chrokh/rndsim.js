const weighted = (alpha, one, two) => {
  const beta = 1 - alpha
  return (one * alpha) + (two * beta)
}

const equally = (values) =>
  values.
    map(v => v * (1 / values.length)).
    reduce((sum, n) => sum + n, 0)

const max = (values) =>
  values.reduce((max, item) =>
    max > item ? max : item, undefined)



module.exports = {
  weighted,
  equally,
  max,
}
