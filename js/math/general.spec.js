const expect = require('chai').expect

const isPowerOfTwo = require('./general').isPowerOfTwo

describe('Math.General.isPowerOfTwo', () => {

  it('knows powers', () => {
    expect(isPowerOfTwo(0)).to.eq(true)
    expect(isPowerOfTwo(1)).to.eq(true)
    expect(isPowerOfTwo(2)).to.eq(true)
    expect(isPowerOfTwo(3)).to.eq(false)
    expect(isPowerOfTwo(4)).to.eq(true)
    expect(isPowerOfTwo(12)).to.eq(false)
    expect(isPowerOfTwo(13)).to.eq(false)
    expect(isPowerOfTwo(64)).to.eq(true)
    expect(isPowerOfTwo(255)).to.eq(false)
    expect(isPowerOfTwo(256)).to.eq(true)
  })

})
