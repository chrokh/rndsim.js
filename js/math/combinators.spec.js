const expect = require('chai').expect
const max = require('./combinators').max

describe('Math.Combinators.max', () => {

  it('picks max', () => {
    expect(max([])).to.eq(undefined)
    expect(max([100])).to.eq(100)
    expect(max([100, 200])).to.eq(200)
    expect(max([100, 200, 50])).to.eq(200)
  })

})
