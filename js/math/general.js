// source: http://stackoverflow.com/questions/9259952/determine-if-number-is-in-the-binary-sequence-1-2-4-8-16-32-64-etc
const isPowerOfTwo = (i) =>
  (i & (i - 1)) === 0

const maxOf = (list) =>
  list.reduce((max, n) => max > n ? max : n, undefined)

const minOf = (list) =>
  list.reduce((min, n) => min < n ? min : n, undefined)



module.exports = {
  isPowerOfTwo,
  maxOf,
  minOf,
}
