const Cache = require('./cache')


module.exports = class Store {

  constructor(state={}) {
    this.state = state
    this.state.index = this.state.index || {}
    this.kvp   = {}
    this.cache = new Cache()
  }

  select(f, ...args) {
    return f(this.state, ...args)
  }

  query(f, ...args) {
    return f(this.state, this.cache, ...args)
  }

  dispatch(f, ...args) {
    return this.state = f(this.state, ...args)
  }

  get(key) {
    return this.kvp[key]
  }

  set(key, value) {
    this.kvp[key] = value
  }

  cacheGrab(key, f) {
    return this.cache.grab(key, f)
  }

}
