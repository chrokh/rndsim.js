const combine   = require('../math/combinators').weighted
const maxOf     = require('../math/general').maxOf
const intervene = require('../interventions/intervene')
const reducedByPatentLife = require('../calculations/periods').reducedByPatentLife



module.exports = (project, interventions = [], remainingPatent, valuate) => {

  // Evaluate without interventions
  let max = valuate(reducedByPatentLife(project.steps, remainingPatent))

  // Evaluate with every available intervention
  for (const intervention of interventions) {
    const value = valuate(intervene(project, intervention, remainingPatent)(project.steps), intervention)
    if (value > max) max = value
  }

  return max
}
