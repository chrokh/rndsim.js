const _            = require('lodash')
const expect       = require('chai').expect
const Simulation   = require('./simulation')
const serialize    = require('./serializers/projects')
const merge        = require('./utils').merge


describe('Simulation', function() {
  this.timeout(90000)

  before(() => {
    this.input = {
      "seed":            123,
      "tick":            0,
      "number_of_runs":  4,
      "number_of_ticks": 25,
      "cycle":           1,

      "stage_groups":        [ "S1", "S2", "M1" ],
      "project_groups":      [ "A", "B" ],

      "intervention_types":  [ ],

      "organization_types": [
        {
          "id":                "Org1",
          "cost_of_capital":   0.03,
          "compounder":        1,
          "threshold":         0,
          "infinite_capital":  false,
          "spawn":             0
        },
        {
          "id":                "Org2",
          "cost_of_capital":   0.03,
          "compounder":        1,
          "threshold":         0,
          "infinite_capital":  true,
          "spawn":             20
        }
      ],

      "investor_types":      [
        {
          "id":               "VC1",
          "cost_of_capital":  0,
          "compounder":       1,
          "threshold":        0,
          "fraction":         1,
          "spawn":            1,
          "targets":          [ "S1", "S2" ]
        }
      ],

      "researchers": [
        {
          "id":            "Researcher",
          "discoverables": [ "ProjA" ]
        }
      ],

      "project_types": [
        {
          "id":              "ProjA",
          "lifecycle":       "Lifecycle",
          "group":           "A",
          "first_owner":     "Org1",
          "max_hibernation": 10,
          "discovery_rate":  "Discoverable",
          "spawn":           0
        },
        {
          "id":              "ProjB",
          "lifecycle":       "Lifecycle",
          "group":           "B",
          "first_owner":     "Org1",
          "max_hibernation": 10,
          "discovery_rate":  "Undiscoverable",
          "spawn":           6
        },
      ],

      "lifecycles": [
        {
          "id": "Lifecycle",
          "completed": [ ],
          "remaining": [ "Stage1", "Stage2", "Market1" ]
        }
      ],

      discovery_rates: [
        { id: "Discoverable",   rate: 20 },
        { id: "Undiscoverable", rate: 0 },
      ],

      "stage_types": [
        {
          "id": "Stage1",
          "group": "S1",
          "point_of_interest": false,
          "requires_patent":   true,
          "prob": 0.9,
          "cost": 10,
          "cash": 0,
          "time": [1, 5],
          "correction": 1
        },
        {
          "id": "Stage2",
          "group": "S2",
          "point_of_interest": false,
          "requires_patent":   true,
          "prob": 0.9,
          "cost": 10,
          "cash": 0,
          "time": 2,
          "correction": 1
        },
        {
          "id": "Market1",
          "group": "M1",
          "point_of_interest": true,
          "requires_patent":   true,
          "prob": 1,
          "cost": 0,
          "cash": [0, 25, 100],
          "time": 2,
          "correction": 1
        }
      ],

      grant_types: [
        {
          size:     1000,
          cycle:    1,
          fraction: 0.25,
          pools: [
            {
              id:               "G2",
              target_stages:    [ "S1" ],
              target_projects:  [ "A", "B" ],
            }
          ],
        }
      ]
    }
  })


  context('in the test case, behaves such that', () => {

    before((done) => {
      var simulation = new Simulation(this.input)
      let output = []
      let donned = 0 // TODO: There should exist an all-runs-done hook
      simulation.on('beforeTick', (store) => { output = output.concat(serialize(store.state, store.cache)) })
      simulation.on('after', () => { donned++ })
      simulation.on('after', () => { if (donned == this.input.number_of_runs) { this.output = output } })
      simulation.on('after', () => { if (donned == this.input.number_of_runs) { done() } })
      simulation.loop()
    })

    context('ticks', () => {
      it('span from the start tick and to the number of ticks, both specified in the input, inclusive', () => {
        const expected = _.range(this.input.tick, this.input.number_of_ticks)
        expect(uniq(this.output.map(l => l.TICK))).to.eql(expected)
      })
    })

    context('projects', () => {
      it('are discovered according to the discovery rates on every tick except the initial tick', () => {
        const runs          = this.input.number_of_runs
        const ticks         = this.input.number_of_ticks - 1
        const initials      = { A: 0,  B: 6 }
        const discoveryRate = { A: 20, B: 0 }
        const subset        = {
          A: this.output.filter(l => l.proj_group == 'A'),
          B: this.output.filter(l => l.proj_group == 'B')
        }
        expect(uniq(subset.A.map(l => l.PROJ)).length).to.eq(runs * initials.A + runs * (ticks) * discoveryRate.A)
        expect(uniq(subset.B.map(l => l.PROJ)).length).to.eq(runs * initials.B + runs * (ticks) * discoveryRate.B)
      })

      it('can be found in all possible states defined in the input', () =>
        expect(uniq(this.output.map(l => l.proj_state)).sort()).to.eql([
          "COMPLETED", "DECIDING", "DEVELOPING", "EXHAUSTED", "FAILED", "HIBERNATING", "INITIALIZING", "TERMINATED" ]))

      it('have any of the possible discovery rates defined in the input', () =>
        expect(uniq(this.output.map(l => l.proj_type_discovery_rate)).sort()).to.eql([0, 20]))

      it ('are either only discovered (ORGANIC) or part of the initial state (INITIAL)', () =>
        expect(uniq(this.output.map(l => l.proj_source)).sort()).to.eql(['INITIAL', 'ORGANIC']))

      xit ('are marked as ORGANIC when discovered in the simulation')
      xit ('are marked as INITIAL when part of initial state')
    })

    context ('owners', () => {
      it ('are created whenever projects are created', () => {
        const baseline    = this.input.number_of_runs * 6
        const discoveries = this.input.number_of_runs * 38 * 10
        const expected    = baseline + discoveries
        expect(uniq(this.output.map(l => l.proj_owner)).length).to.be.at.least(expected)
        // TODO: Hard-coded numbers should be generalized
      })

      it('are of any of the organization types defined in the input, and all types are represented', () =>
        expect(uniq(this.output.map(l => l.proj_owner_type)).sort()).to.eql(['Org1', 'Org2']))
    })

    context('composite valuation (ENPV) per project', () => {
      it('is a number within a reasonable range', () => {
        for (line of this.output) {
          expect(line.proj_composite_valuation).to.be.at.least(-100)
          expect(line.proj_composite_valuation).to.be.at.most(100)
        }
      })

      it('is sometimes negative', () => {
        expect(min(this.output.map(l => l.proj_composite_valuation))).to.be.below(0)
      })

      it('is not always the same number', () => {
        expect(uniq(this.output.map(l => l.proj_composite_valuation)).length).to.be.above(1)
      })
    })

    context('capital spent per project', () => {
      before(() => {
        const stages    = 2
        const stageCost = 10
        const fraction  = 0.8
        this.expectedMin = 0
        this.expectedMax = stages * stageCost * fraction
        this.actual = uniq(this.output.map(l => l.proj_grants_spent))
      })

      it('is between 0 and the total max cost of all stages', () => {
        const stages    = 2
        const stageCost = 10
        const expectedMin = 0
        const expectedMax = stages * stageCost
        expect(min(uniq(this.output.map(l => l.proj_capital_spent)))).to.eql(expectedMin)
        expect(max(uniq(this.output.map(l => l.proj_capital_spent)))).to.eql(expectedMax)
      })

      it('never is 0, when beyond the first stage', () => {
        const filtered = this.output.filter(l => l.proj_stage_group != 'S1')
        expect(this.output.length).to.be.above(filtered.length)
        expect(min(uniq(filtered.map(l => l.proj_capital_spent)))).to.be.above(0)
      })
    })

    context('grants spent per project', () => {
      before(() => {
        const stages    = 2
        const stageCost = 10
        const fraction  = 0.8
        this.expectedMin = 0
        this.expectedMax = stages * stageCost * fraction
        this.actual = uniq(this.output.map(l => l.proj_grants_accumulated))
      })

      it('is between 0 and the max grants accumulatable (i.e. stage cost * top grant fraction, for all stages)', () => {
        expect(min(this.actual)).to.be.at.least(this.expectedMin)
        expect(max(this.actual)).to.be.at.most(this.expectedMax)
      })

      it('does happen to be values between the extreme min and max cases', () => {
        const filtered = this.actual.filter(x => x != this.expectedMin && x != this.expectedMax)
        expect(filtered.length).to.be.below(this.actual.length)
        expect(filtered.length).to.be.above(0)
      })

      it('is never at any given time higher than grants accumulated', () => {
        for (const line of this.output) {
          expect(line.proj_grants_accumulated).to.be.at.least(line.proj_grants_spent)
        }
      })
    })

    context('grants accumulated per project', () => {
      before(() => {
        const stages    = 2
        const stageCost = 10
        const fraction  = 0.8
        this.expectedMin = 0
        this.expectedMax = stages * stageCost * fraction
        this.actual = uniq(this.output.map(l => l.proj_grants_spent))
      })

      it('is between 0 and the max grants accumulatable (i.e. stage cost * top grant fraction, for all stages)', () => {
        expect(min(this.actual)).to.be.at.least(this.expectedMin)
        expect(max(this.actual)).to.be.at.most(this.expectedMax)
      })

      it('does happen to be values between the extreme min and max cases', () => {
        const filtered = this.actual.filter(x => x != this.expectedMin && x != this.expectedMax)
        expect(filtered.length).to.be.below(this.actual.length)
        expect(filtered.length).to.be.above(0)
      })

      xit('is never received twice in the same stage', () => {})
    })

    context ('grants', () => {
      xit('always are used before investment money')
    })

    context('average grants fraction', () => {
      xit('is of the values specified in input and all specified are represented')
    })

    context('total grants size', () => {
      xit('is of the values specified in input and all specified are represented')
    })

    context('number of partners per project', () => {
      it('is never more than 1', () =>
        expect(max(this.output.map(l => l.proj_partners))).to.eql(1))

      it('is sometimes 0 and never below', () =>
        expect(min(this.output.map(l => l.proj_partners))).to.eql(0))
    })


    context ('investment capital accumulated per project', () => {
      it ('is sometimes greater than 0', () =>
        expect(max(this.output.map(l => l.proj_capital_accumulated))).to.be.above(0))

      it('is less than the highest stages costs times the highest funding fractions for all stages', () =>
        expect(max(this.output.map(l => l.proj_capital_accumulated))).to.be.at.most(20))
    })

    context('number of investors per project (assuming that all investors fund 100% of the next stage)', () => {
      it('is either 0 or any number equal to or below the number of stages', () =>
        expect(uniq(this.output.map(l => l.proj_investors)).sort()).to.eql([0,1,2]))

      xit('never increases within the same stage (assuming that all investors fund 100% of the next stage)', () => {
        const actual = {}
        for (const line of this.output) {
          const project = line.PROJ
          const stage   = line.proj_stage_group
          const count   = line.proj_investors
          actual[project] = actual[project] || {}
          if (actual[project][stage] == undefined)
            actual[project][stage] = count
          else {
            if (count > actual[project][stage] + 1) {
              expect(count).to.be.at.least(actual[project][stage])
              expect(count).to.be.at.most(actual[project][stage] + 1)
            }
          }
        }
      })
    })

  })



  context('yields different outcomes under different input', () => {

    before(() => {
      this.input = Object.assign({}, this.input, { number_of_runs: 5 })
    })

    xit('more entries when run with top-up intervention then when run without')
    xit('more entries when run with top-up intervention than when with delinkage then when run without')
    xit('more entries when market is larger')
    xit('more entries when development time is shorter')
    xit('more entries when investors have lower demands')
    xit('more entries when everyone has infinite capital')
    xit('more entries when development years and initial market years do not require patent')

    context('such that full delinkage', () => {
      it('gives more entries with intervention than without', (done) => {
        expectImprovement(done, this.input,
          inputWithIntervention(this.input, { size: 10000 }))
      })

      it('gives more entries when intervention is larger', (done) => {
        expectImprovement(done,
          inputWithIntervention(this.input, { size: 0 }),
          inputWithIntervention(this.input, { size: 100 }))
      })

      it('does not give more entries with intervention than without if the intervention size is 0', (done) => {
        expectNoImprovement(done, this.input,
          inputWithIntervention(this.input, { size: 0 }))
      })

      it('does not give more when intervention targets non-existing stage group', (done) => {
        expectNoImprovement(done, this.input,
          inputWithIntervention(this.input, { size: 10000, targetStage: 'non-existant' }))
      })
    })

    context('such that partial delinkage', () => {
      it('gives more entries with intervention than without', (done) => {
        expectImprovement(done, this.input,
          inputWithIntervention(this.input, { strategy: 'ADD', size: 10000 }))
      })

      it('gives more entries when intervention is larger', (done) => {
        expectImprovement(done,
          inputWithIntervention(this.input, { strategy: 'ADD', size: 0 }),
          inputWithIntervention(this.input, { strategy: 'ADD', size: 100 }))
      })

      it('does not give more entries with intervention than without if the intervention size is 0', (done) => {
        expectNoImprovement(done, this.input,
          inputWithIntervention(this.input, {strategy: 'ADD', size: 0 }))
      })

      it('does not give more when intervention targets non-existing stage group', (done) => {
        expectNoImprovement(done, this.input,
          inputWithIntervention(this.input, { strategy: 'ADD', size: 10000, targetStage: 'non-existant' }))
      })

      it('gives more higher valuations than full delinkage (all else equal, and when size is at the sweet spot)', (done) => {
        expectImprovement(done,
          inputWithIntervention(this.input, { strategy: 'REPLACE', size: 24.5 }),
          inputWithIntervention(this.input, { strategy: 'ADD',     size: 24.5 }),
        meanValuation)
      })

    })

    it('more entries when grants are introduced', (done) => {
      const withoutGrants = this.input
      const withGrants = Object.assign({}, this.input, {
        grant_types: [
          {
            size:     1000,
            cycle:    1,
            fraction: 1,
            pools: [
              {
                id:               "G2",
                target_stages:    [ "S1", "S2" ],
                target_projects:  [ "A", "B" ],
              }
            ],
          }
        ]
      })
      expect(withoutGrants).to.not.eql(withGrants)
      expectImprovement(done, withoutGrants, withGrants, meanValuation)
    })

  })



})


const inputWithIntervention = (input, opts = {}) => merge(input, {
  stage_types: [
    {
      "id":                 "MER",
      "group":              "Intervention",
      "point_of_interest":  true,
      "requires_patent":    false,
      "correction":         1,
      "prob":               1,
      "cost":               0,
      "time":               3,
      "cash":               opts.size,
    }
  ],
  intervention_types: [
    {
      "id":               "Intervention",
      "target_stage":     (opts.targetStage || 'M1'),
      "strategy":         (opts.strategy || 'REPLACE'),
      "stage_types":      [ "MER" ],
      "target_projects":  [ "A" ]
    }
  ]
})



const countEntries = (_output) =>
  uniq(_output.filter(l => l.proj_state == 'COMPLETED').map(l => l.PROJ)).length

const meanValuation = (_output) =>
  mean(_output.map(l => l.proj_composite_valuation))

const expectImprovement = (done, input1, input2, f = countEntries) => {
  run(input1, (result1) => {
    run(input2, (result2) => {
      expect(f(result1)).to.be.above(0)
      expect(f(result2)).to.be.above(0)
      expect(f(result2)).to.be.above(f(result1))
      done()
    })
  })
}

const expectNoImprovement = (done, input1, input2, f = countEntries) => {
  run(input1, (result1) => {
    run(input2, (result2) => {
      expect(f(result1)).to.be.above(0)
      expect(f(result2)).to.be.above(0)
      expect(f(result2)).to.eq(f(result1))
      done()
    })
  })
}

const uniq = (items) =>
  items.reduce((acc, item) => acc.indexOf(item) == -1 ? acc.concat([item]): acc, [])

const min = (items) =>
  items.reduce((min, item) => min < item ? min : item)

const max = (items) =>
  items.reduce((max, item) => max > item ? max : item)

const sum = (items) =>
  items.reduce((tot, item) => tot + item, 0)

const mean = (items) =>
  sum(items) / items.length

const run = (_input, _f) => {
  let dones = 0
  const simulation = new Simulation(_input)
  let result = []
  simulation.on('beforeTick', (store) =>
    result = result.concat(serialize(store.state, store.cache)))
  simulation.on('after', () => { dones++ })
  simulation.on('after', () => { if (dones == _input.number_of_runs) { _f(result) } })
  simulation.loop()
}
