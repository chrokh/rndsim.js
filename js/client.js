import 'babel-polyfill'
import example from '../examples/input.json'
import schema  from './schema.json'

export {
  example,
  schema
}

