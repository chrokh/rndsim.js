const fs   = require('fs')
const path = require('path')


module.exports = class CSVWriter {

  constructor (folderPath, options) {
    this.options    = options
    this.folderPath = folderPath
    this.history    = {}
  }


  write (fileName, data) {
    if (data.length == 0) return

    const file = path.join(this.folderPath, fileName + '.csv')

    if (this._hasWrittenTo(fileName) || !this.options.writeHeaders) {
      this._write(file, data)
    } else {
      this._markAsWrittenTo(fileName)
      fs.writeFile(file, _headers(data), () => {
        this._write(file, data)
      })
    }
  }


  _write (file, data) {
    fs.appendFile(file, _lines(data), (err) => {
      if (err) throw err
    })
  }

  _hasWrittenTo (fileName) {
    return this.history[fileName] || false
  }

  _markAsWrittenTo (fileName) {
    this.history[fileName] = true
  }

}


const _headers = (data) =>
  Object.keys(_array(data)[0]) + '\r\n'

const _lines = (data) =>
  _array(data).
    map(line => _line(line)).
    join('\r\n') + '\r\n'


const _line = (data) =>
  Object.values(data).
    join(',').
    replace(',,', ',UNDEFINED,')

const _array = (maybe) =>
  Array.isArray(maybe) ? maybe : [ maybe ]
