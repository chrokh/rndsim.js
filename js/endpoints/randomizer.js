const Random   = require('./random.js')
const RandomJS = require('random-js')

module.exports = class Randomizer {

  /*
   * Distributions
   */

  constructor (seed) {
    this.seed   = seed
    this.lastId = 0

    // SimJS random
    this.random = new Random(seed)

    // random-js
    this.randomjs = new RandomJS(RandomJS.engines.mt19937().seed(seed))
  }


  uniform(min, max) {
    return this.random.uniform(min, max)
  }


  triangular(min, mid, max) {
    return this.random.triangular(min, max, mid)
  }


  integer (min, max) {
    return this.randomjs.integer(min, max)
  }


  points (points) {
    if (isNaN(points[0])) {
      let rand = this.uniform(0, 1)
      let sum = 0
      for (const point of points) {
        const upper = sum + point.prob
        if (rand < upper) {
          return this.sample(point.value).value
        } else {
          sum = sum + point.prob
        }
      }
    } else {
      return this.pick(points)
    }
  }


  // TODO: Should be refactored so that it only
  // returns the value and not { value, distribution }
  sample (dist) {
    let value
    if (Array.isArray(dist)) {
      value = this.points(dist)
    } else if (dist.from != null) {
      dist  = this._unsequence(dist.from, dist.to, dist.by)
      value = this.points(dist)
    } else if (dist.mid != null) {
      value = this.triangular(dist.min, dist.mid, dist.max)
    } else if (dist.min != null) {
      value = this.uniform(dist.min, dist.max)
    } else {
      value = dist // which is actually number
    }
    return { value, distribution: dist }
  }


  /*
   * Helpers
   */

  flip (probability) {
    return this.random.uniform(0, 1) <= probability
  }

  multiflip (probability) {
    const decimals = probability - Math.floor(probability)
    if (this.flip(decimals)) {
      return Math.ceil(probability)
    } else {
      return Math.floor(probability)
    }
  }

  try (probability, f) {
    if (this.flip(probability)) f()
  }

  pick (items) {
    return items[this.integer(0, items.length - 1)]
  }

  shuffle (items) {
    items = items.slice()
    const random = []
    while (items.length > 0) {
      const index = this.integer(0, items.length - 1)
      random.push(items[index])
      items.splice(index, 1)
    }
    return random
  }



  /*
   * Identity
   */

  nextId () {
    this.lastId = this.lastId + 1
    return this.seed + '-' + this.lastId
  }


  /*
   * Helpers
   */

  _unsequence (from, to, by) {
    let points = []
    let last   = from
    do {
      points.push(last)
      last += by
    } while (last <= to)
    return points
  }


}

