const serializeRun = require('../serializers/run')


module.exports = class Sender {

  constructor (writer, serializers) {
    this.writer      = writer
    this.serializers = serializers
  }

  send (state, cache) {
    const data = {}
    for(const key in this.serializers) {
      data[key] = this.serializers[key](state, cache)
    }

    for (const key in data) {
      this.writer.write(key, data[key])
    }
  }

}
