const expect  = require('chai').expect
const Randomizer = require('./randomizer')
const _ = require('lodash')


describe('Randomizer.sample', () => {

  it('test', () => {
    const randomizer = new Randomizer(123)
    test(randomizer, 80, 2.11, 1, [
        { "prob": 0.1, "value": { "min": 19.0, "mid": 21.10, "max": 23.20 } },
        { "prob": 0.9, "value": 0 },
    ])
  })

  it('test', () => {
    const randomizer = new Randomizer(123)
    test(randomizer, 40, 2.11, 1, [
        { "prob": 0.9, "value": 0 },
        { "prob": 0.1, "value": { "min": 19.0, "mid": 21.10, "max": 23.20 } },
    ])
  })

  xit('handles discrete distributions', () => {
    const randomizer = new Randomizer(123)
    test(randomizer, 100, 190, 1, [
      { "prob": 0.1, "value": 100 },
      { "prob": 0.9, "value": 200 }
    ])
  })

  xit('handles discrete distributions', () => {
    const randomizer = new Randomizer(123)
    test(randomizer, 100000, 21070, 2, [
      { "prob": 0.1, "value": 200 },
      { "prob": 0.1, "value": 400 },
      { "prob": 0.7, "value": 30000 },
      { "prob": 0.1, "value": 100 },
    ])
  })

  it('handles discrete distributions', () => {
    const randomizer = new Randomizer(123)
    test(randomizer, 100000, 280, 2, [
      { "prob": 0.1, "value": 200 },
      { "prob": 0.1, "value": 400 },
      { "prob": 0.7, "value": 300 },
      { "prob": 0.1, "value": 100 },
    ])
  })

  it('handles discrete distributions', () => {
    const randomizer = new Randomizer(123)
    test(randomizer, 1000, 15.095, 0.5, [
      { "prob": 0.1, "value": 0 },
      { "prob": 0.1, "value": 12 },
      { "prob": 0.7, "value": 17.45 },
      { "prob": 0.1, "value": 16.8 },
    ])
  })

  xit('handles discrete distributions of distributions', () => {
    const randomizer = new Randomizer(123)
    test(randomizer, 100000, 1630, 1, [
      { "prob": 0.1, "value": { min: 100, max: 200 } },
      { "prob": 0.8, "value": { min: 1000, mid: 2000, max: 3000 } },
      { "prob": 0.1, "value": { min: 10, mid: 20, max: 30 } },
    ])
  })

  xit('handles discrete distributions of distribution', () => {
    const randomizer = new Randomizer(123)
    test(randomizer, 100000, 46, 1, [
      { "prob": 0.2, "value": { min: 100, max: 200 } },
      { "prob": 0.5, "value": { min: 10, mid: 20, max: 30 } },
      { "prob": 0.3, "value": 10 },
    ])
  })

  it('handles a real world scenario', () => {
    const randomizer = new Randomizer(123)
    test(randomizer, 500, 19.86, 1, [
      { "prob": 0.7, "value": { "min": 17.45, "mid": 24.0, "max": 30.37 }  },
      { "prob": 0.1, "value": 0 },
      { "prob": 0.1, "value": {"min": 8.725, "mid": 12.0, "max": 15.185 }  },
      { "prob": 0.1, "value": { "min": 12.215, "mid": 16.8, "max": 21.259 } },
    ])
  })

  it('handles a real world scenario', () => {
    const randomizer = new Randomizer(123)
    const dist = [
      { "prob": 0.1, "value": 0 },
      { "prob": 0.7, "value": { "min": 17.27, "mid": 24.55, "max": 37.09 } },
      { "prob": 0.1, "value": { "min": 8.635, "mid": 12.275, "max": 18.545 } },
      { "prob": 0.1, "value": { "min": 12.089, "mid": 17.185, "max": 25.963 } },
    ]
    test(randomizer, 50, 20.131, 1, dist)
  })

  it('handles a real world scenario', () => {
    const randomizer = new Randomizer(123)
    const dist = [
        { "prob": 0.7, "value": { "min": 19.0, "mid": 21.10, "max": 23.20 } },
        { "prob": 0.1, "value": 0 },
        { "prob": 0.1, "value": { "min": 9.5, "mid": 10.55, "max": 11.6 } },
        { "prob": 0.1, "value": { "min": 13.3, "mid": 14.77, "max": 16.24 } }
    ]
    test(randomizer, 500, 17.302, 1, dist)
  })

  it('converts sequence definitions to a list of points', () => {
    const randomizer = new Randomizer(123)
    test(randomizer, 500, 30, 0.5, { from: 10, to: 50, by: 10 })   // even
    test(randomizer, 500, 26.5, 0.5, { from: 10, to: 50, by: 11 }) // odd
    expect(randomizer.sample({ from: 4, to: 10, by: 2}).distribution).to.eql([4, 6, 8, 10])
  })

})



describe('Randomizer.multiflip', () => {

  it('averages to the exepcted average', () => {
    const randomizer = new Randomizer(123)
    const numbers = trials(100, () => randomizer.multiflip(4.256))
    expect(diff(mean(numbers), 4.256)).to.be.below(0.05)
  })

  it('averages to the exepcted average', () => {
    const randomizer = new Randomizer(123)
    const numbers = trials(1000, () => randomizer.multiflip(4.8))
    expect(diff(mean(numbers), 4.8)).to.be.below(0.05)
  })

  it('maintains an appropriate ratio', () => {
    const randomizer = new Randomizer(123)
    const numbers = trials(100, () => randomizer.multiflip(4.25))
    const fours = numbers.filter(n => n == 4).length
    const fives = numbers.filter(n => n == 5).length
    expect(diff(fours, 75)).to.be.at.most(1)
    expect(diff(fives, 25)).to.be.at.most(1)
  })

})


const test = (randomizer, n, expected, accuracy, distribution) => {
  const actual = calculate(n, distribution, randomizer)
  expect(diff(expected, actual)).to.be.below(accuracy)
}

const diff = (a, b) =>
  Math.abs(a - b)

const trials = (n, f) =>
  _(n).times(n => f())

const mean = (numbers) =>
  numbers.reduce((sum, n) => sum + n, 0) / numbers.length

const max = (numbers) =>
  numbers.reduce((max, n) => max > n ? max : n)

const min = (numbers) =>
  numbers.reduce((min, n) => min < n ? min : n)

const calculate = (n, distribution, randomizer) =>
  _(n).times((n) =>
    randomizer.sample(distribution).value
  ).reduce((sum, n) => sum + n, 0) / n
