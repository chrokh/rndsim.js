const _ = require('lodash')

const validate      = require('./synthesizers/validate.js')
const normalize     = require('./synthesizers/normalizer.js').normalize

const Randomizer    = require('./endpoints/randomizer.js')
const Store         = require('./store')

const numberOfTicks = require('./selectors/simulation').numberOfTicks
const currentTick   = require('./selectors/simulation').currentTick
const incrementTick = require('./actions/simulation').incrementTick

const recalculateCostOfCapital = require('./rules/recalculate-cost-of-capital')
const correctForInflation  = require('./rules/correct-for-inflation')
const sampleGrants         = require('./rules/sample-grants')
const sample               = require('./rules/sample')
const spawnProjects        = require('./rules/spawn-projects')
const spawnInterventions   = require('./rules/spawn-interventions')
const spawnOrganizations   = require('./rules/spawn-organizations')
const purgeOrganizations   = require('./rules/purge-organizations')
const discovery            = require('./rules/discovery')
const offspinning          = require('./rules/offspinning')
const grants               = require('./rules/grants')
const stateChanges         = require('./rules/state-changes')
const initialization       = require('./rules/initialization')



module.exports = (handler) => {
  //handler.finish('done with ' + handler.init)
  //setTimeout(() => progress(20), 100)
  //setTimeout(() => progress(40), 300)
  //setTimeout(() => done({ run: input.run, store: {} }), 1000)

  const run = new Run(input, config)
  run.start()
  handler.send(handler.init)
}



class Run {

  constructor(input, config) {
    this.input  = input

    this.callbacks = {
      before:      config.before     || [],
      beforeTick:  config.beforeTick || [],
      tick:        config.tick       || [],
      afterTick:   config.afterTick  || [],
      after:       config.after      || [],
    }

    // Create randomizer
    this.randomizer = new Randomizer(input.seed + input.run)

    // Add initiation rules
    this.on('before', correctForInflation)
    this.on('before', recalculateCostOfCapital)
    this.on('before', sample)
    this.on('before', sampleGrants)
    this.on('before', spawnOrganizations)
    this.on('before', spawnInterventions)
    this.on('before', spawnProjects)

    // Add rules
    this.on('tick', purgeOrganizations)
    this.on('tick', grants)
    this.on('tick', stateChanges)
    this.on('tick', discovery)
    this.on('tick', offspinning)
    this.on('tick', initialization) // Set project states for next tick
  }


  on(name, callback) {
    this.callbacks[name].push(callback);
  }


  start() {
    this.store = new Store(normalize(validate(this.input)))
    this.callbacks.before.forEach(cb => { cb(this.store, this.randomizer) })
    this.step()
  }


  step() {
    this.callbacks.beforeTick.forEach(f => f(this.store, this.randomizer))
    this.callbacks.tick.forEach(f => f(this.store, this.randomizer))
    this.store.dispatch(incrementTick)
    this.callbacks.afterTick.forEach(f => f(this.store, this.randomizer))
    if (this.isDone()) {
      this.callbacks.after.forEach(f => { f(this.store, this.randomizer) })
    } else {
      this.step()
    }
  }


  isDone() {
    const current = this.store.select(currentTick)
    const target  = this.store.select(numberOfTicks)
    return current > target
  }

}
