module.exports = {

  Simulation:  require('./simulation'),

  Endpoints: {
    CSVWriter: require('./endpoints/csv-writer'),
    Sender:    require('./endpoints/sender'),
  },

  Selectors: {
    Simulation: {
      currentTick:      require('./selectors/simulation').currentTick,
      currentRun:       require('./selectors/simulation').currentRun,
      isAtStartOfCycle: require('./selectors/simulation').isAtStartOfCycle,
    }
  },

}
